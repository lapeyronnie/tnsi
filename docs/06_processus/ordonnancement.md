---
author: Jason LAPEYRONNIE
title: Ordonnancement des processus
---

Dans un système multi-utilisateurs à temps partagé, plusieurs processus peuvent être présents en mémoire centrale en attente d’exécution. Si plusieurs processus sont prêts, le système d’exploitation doit gérer l’allocation du processeur aux différents processus à exécuter. C’est l’ordonnanceur qui s’acquitte de cette tâche.

## Ordonnancements First-Come First-Served et Shortest Job First

Dans un système à ordonnancement non préemptif ou sans réquisition, le système d’exploitation choisit le prochain processus à exécuter, en général, le Premier Arrivé est le Premier Servi PAPS (ou First-Come First-Served FCFS) ou le plus court d’abord (Short Job First SJF). Il lui alloue le processeur jusqu’à ce qu’il se termine ou qu’il se bloque (en attente d’un événement). Il n’y a pas de réquisition. Si l’ordonnanceur fonctionne selon la stratégie SJF, il choisit, parmi le lot de processus à exécuter, le plus court (plus petit temps d’exécution). Cette stratégie est bien adaptée au traitement par lots de processus dont les temps maximaux d’exécution sont connus ou fixés par les utilisateurs car elle offre un meilleur temps moyen de séjour. Le temps de séjour d’un processus (temps de rotation ou de virement) est l’intervalle de temps entre la soumission du processus et son achèvement.

Exemple : Considérons cinq processus notés A, B, C, D et E, dont les temps d’exécution et leurs arrivages respectifs sont donnés dans le tableau ci-dessous.

![Tableau](tab01.png)

On peut également représenter le tableau précédent de la manière suivante afin de faciliter la compréhension de l’ordonnancement des processus :

![Tableau](tab02.png)

### Algorithme “Premier arrivé premier servi” : First-Come First-Served (FCFS)

**Schéma d’exécution :**

![Tableau](tab03.png)

Au temps 0, seulement le processus A est dans le système et il s’exécute. Au temps 1 le processus B arrive mais il doit attendre que A se termine car il lui reste encore 2 unités de temps à effectuer. Ensuite B s’exécute pendant 4 unités de temps. Au temps 4, 6, et 7 les processus C, D et E arrivent mais B a encore 2 unités de temps. Une fois que B a terminé, C, D et E entrent dans le système dans l’ordre (on suppose qu’il y a aucun blocage).

Le temps de séjour pour chaque processus est obtenu soustrayant le temps d’entrée du processus du temps de 
terminaison. Ainsi :

![Tableau](tab04.png)

Le **temps moyen de séjour** est : (3 + 8 + 9 + 9 + 9) / 5 = 38 / 5 = 7,6.

Le **temps d’attente** est calculé soustrayant le temps d’exécution du temps de séjour :

![Tableau](tab05.png)

Le **temps moyen d’attente** est : (0 + 2 + 5 + 7 + 8) / 5 = 23 /5 = 4,4

Il y a cinq tâches exécutées dans 16 unités de temps, alors 16 / 5 = 3,2 unités de temps par processus.

### Algorithme “Le plus court d’abord” : Short Job First (SJF)

**Schéma d’exécution :**

![Tableau](tab06.png)

Le processeur traite d’abord comme précédemment les processus A puis B. Le processus B s’achève à la date t = 9 qui est supérieure au temps d’arrivage des processus C, D et E : ceux-ci sont donc déjà présents dans la pile des processus en attente de traitement par le processeur. Celui-ci choisi d’abord d’exécuter le processus le plus court des 3 c’est-à-dire E conformément à l’algorithme SJF. Puis selon la même logique, il exécute le processus D et enfin le processus C.

**Temps de séjour :**

![Tableau](tab07.png)

**Temps moyen de séjour** : (3 + 8 + 3 + 6 + 12) / 5 = 32 / 5 = 6,4

**Temps d’attente :**

![Tableau](tab08.png)

**Temps moyen d’attente** : (0 + 2 + 2 + 4 + 8) / 5 = 16 / 5 = 3,2

Il y a cinq tâches exécutées dans 16 unités de temps, alors 16 / 5 = 3,2 unités de temps par processus.

On observe que dans ce cas de figure l’algorithme Short Job First (SJF) est plus performant que l’algorithme First-Come First-Served (FCFS).

### Exercice

Dans chacun des cas suivants, construire le schéma d'exécution des processus puis déterminer les temps de séjour, temps moyens de séjour, temps d'attente et temps moyen d'attente des processus selon les algorithmes FCFS et SJF

![Tableau](tab09.png)

![Tableau](tab10.png)

## Ordonnancement préémptif : Shortest Remaining Time (SRT) et Round Robin (RR)

Dans un schéma d’ordonnanceur préemptif, ou avec réquisition, pour s’assurer qu’aucun processus ne s’exécute pendant trop de temps, les ordinateurs ont une horloge électronique qui génère périodiquement une interruption. A chaque interruption d’horloge, le système d’exploitation reprend la main et décide si le processus courant doit poursuivre son exécution ou s’il doit être suspendu pour laisser place à un autre. 

S’il décide de suspendre son exécution au profit d’un autre, il doit d’abord sauvegarder l’état des registres du processeur avant de charger dans les registres les données du processus à lancer. C’est qu’on appelle la commutation de contexte ou le changement de contexte. 

Cette sauvegarde est nécessaire pour pouvoir poursuivre ultérieurement l’exécution du processus suspendu. Le processeur passe donc d’un processus à un autre en exécutant chaque processus pendant quelques dizaines ou centaines de millisecondes. Le temps d’allocation du processeur au processus est appelé **quantum**. 

Cette commutation entre processus doit être rapide, c’est-à-dire, exiger un temps nettement inférieur au quantum. Le processeur, à un instant donné, n’exécute réellement qu’un seul processus, mais pendant une seconde, le processeur peut exécuter plusieurs processus et donne ainsi l’impression de parallélisme (pseudo-parallélisme).

### Algorithme du plus petit temps de séjour (SRT)

L’ordonnancement du plus petit temps de séjour ou Shortest Remaining Time (SRT) est la version préemptive de l’algorithme SJF. Un processus arrive dans la file de processus, l’ordonnanceur compare la valeur espérée pour ce processus contre la valeur du processus actuellement en exécution. Si le temps du nouveau processus est plus petit, il rentre en exécution **immédiatement**.

### Algorithme du tourniquet circulaire (Round Robin)

L’algorithme du tourniquet, circulaire ou **Round Robin (RR)** représenté sur la figure ci-dessous est un algorithme ancien, simple, fiable et très utilisé. Il mémorise dans une file du type **FIFO (First In First Out)** la liste des processus prêts, c’est-à-dire en attente d’exécution.

![Round Robin](rr01.png)


- Choix du processus à exécuter

Il alloue le processeur au processus en tête de file, pendant un quantum de temps. Si le processus se bloque ou se termine avant la fin de son quantum, le processeur est immédiatement alloué à un autre processus (celui en tête de file). Si le processus ne se termine pas au bout de son quantum, son exécution est suspendue. Le processeur est alloué à un autre processus (celui en tête de file). Le processus suspendu est inséré en queue 
de file. Les processus qui arrivent ou qui passent de l’état bloqué à l’état prêt sont insérés en queue de file.

- Choix de la valeur du quantum

Un quantum trop petit provoque trop de commutations de processus et abaisse l’efficacité du processeur. Un quantum trop élevé augmente le temps de réponse des courtes commandes en mode interactif. Un quantum entre 20 et 50 ms est souvent un compromis raisonnable.

**Remarque** : le quantum étatit de 1 s dans les premières versions d’UNIX.

## Un exemple

Soient deux processus A et B prêts tels que A est arrivé en premier suivi de B, 2 unités de temps après. Les temps CPU nécessaires pour l’exécution des processus A et B sont respectivement 15 et 4 unités de temps. Le temps de commutation est supposé nul.

![Round Robin](rr02.png)

### Algorithme SRT

![Round Robin](rr03.png)

### Algorithme RR (quantum = 10 unités de temps)

![Round Robin](rr04.png)

![Round Robin](rr05.png)

### Algorithme RR (quantum = 3 unités de temps)

![Round Robin](rr06.png)

## Observation des processus

Sous Linux il existe des commandes permettant de visualiser les processus : 

- la commande ps utilisée avec les options aef (ps -aef) permet de visualiser les processus en cours un ordinateur, notamment les PID et les PPID de ces processus. Problème : La commande ps ne permet pas de suivre en temps réel les processus (affichage figé). 
- la commande top permet d'avoir un suivi en temps réel des processus. 
- la commande kill permet de supprimer un processus. L'utilisation de cette commande est très simple, il suffit de taper kill suivi du PID du processus à supprimer (exemple : kill 4242 permet de supprimer le processus de PID 4242)

![Round Robin](commandes.png)

La documentation Linux donne la signification des différents champs : 
- UID : identifiant utilisateur effectif ; 
- PID : identifiant de processus ; 
- PPID : PID du processus parent ; 
- C : partie entière du pourcentage d'utilisation du processeur par rapport au temps de vie des processus ; 
- STIME : l'heure de lancement du processus ; 
- TTY : terminal de contrôle 
- TIME : temps d'exécution 
- CMD : nom de la commande du processus
