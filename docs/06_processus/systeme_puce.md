---
author: Jason LAPEYRONNIE
title: Système sur puce
---

La réduction de taille des éléments des circuits électroniques a conduit à l’avènement de systèmes sur puce (SoC pour System on a Chip en anglais) qui regroupent dans un seul circuit nombre de fonctions autrefois effectuées par des circuits séparés assemblés sur une carte électronique. Un tel système sur puce est conçu et mis au point de façon logicielle, ses briques électroniques sont accessibles par des API, comme pour les bibliothèques logicielles.

## De l'ordinateur au smartphone

Dans un ordinateur “classique” tel qu’un PC de bureau, le « hardware » est organisé autour de 4 éléments principaux :

- le processeur (CPU – Central Processing Unit) se charge de réaliser les calculs les plus courants, ceux qui 
permettent par exemple de faire tourner le système d’exploitation ou un navigateur web.
- la mémoire vive (RAM – Random Access Memory) permet d’enregistrer temporairement les donnéestraitées
par le processeur.
- la carte graphique (ou GPU – Graphics Processing Unit) se charge d’afficher une image, qu’elle soit en 2D ou 
bien en 3D comme dans les jeux.
- la carte-mère (Motherboard) permet l’acheminement des données entre les composants (CPU, RAM, GPU, 
disque dur, SSD, cartes réseau …) via des « BUS ».

![Composants d'un PC](pc01.png)

1. CPU surmonté d’un dissipateur thermique (ventirad)
2. Barrettes de RAM
3. GPU
4. Carte mère

 
Depuis le début de l’ère des smartphones et des tablettes, on assiste à l’émergence de systèmes tout-en-un appelés SoC (System on a Chip) afin d’optimiser la miniaturisation et l’intégration des différents composants. Ces derniers sont alors bien mieux interconnectés les uns aux autres, avec par exemple une fréquence processeur qui varie  en fonction de la fréquence de la carte graphique du fait de contraintes thermiques et de consommation. 

Un SoC présente donc une structure complètement inédite par rapport à un ordinateur classique où chaque composant est plus ou moins indépendant.
Un « système sur une puce » est un système complet embarqué sur une seule puce, pouvant comprendre de la mémoire, un ou plusieurs 
microprocesseurs, des périphériques d'interface, ou tout autre composant  nécessaire à la réalisation de la fonction attendue.

On peut intégrer de la logique, de la mémoire (statique, dynamique, flash, ROM, PROM, EPROM, EEPROM), des dispositifs (capteurs) mécaniques, opto-électroniques, chimiques ou biologiques ou des circuits radio…

## Composition d'un SoC

![Composants d'une puce](puce01.png)

### Le processeur (CPU)

Le **processeur** ou « Central Processing Unit » (CPU) est le cœur du SoC. Son fonctionnement est identique à celui d’un 
ordinateur. On y retrouve donc plusieurs cœurs cadencés à différentes fréquences effectuant des threads et stockant 
des informations en cache.

- Les **cœurs** : Un processeur compte généralement plusieurs cœurs, on parle couramment dans la littérature technique de 
dual-core, quad-core ou d’octo-core parfois. Ainsi, ces processeurs se composent respectivement de deux, 
quatre ou huit cœurs. Ceux-ci permettent de lancer en parallèle plusieurs applications de manière simultanée 
(multitâche) et permettent l’utilisation d’application lourde comme des jeux.
- La **fréquence** : La fréquence d’un processeur est le nombre de cycles de calculs qu’il peut effectuer chaque seconde. Elle va 
donc naturellement déterminer la durée d’exécution d’une tâche : plus la fréquence du processeur est élevée,  plus l’exécution d’une tâche est rapide. Mesurée en gigahertz (GHz), celle-ci est souvent différente entre  chaque cœurs.
-  Les **threads** : Les cœurs réalisent ce qu’on appelle un thread, littéralement un fil d’exécution, une tâche qui doit être 
réalisée par le processeur.
- Le **cache** : C’est une petite mémoire rapide intégrée au processeur. En effet, celle-ci va permettre de stocker les
informations récurrentes au plus près du processeur pour éviter d’avoir à aller les chercher sans arrêt dans la 
RAM.

### La puce graphique (GPU)

La **puce graphique** ou « Graphics Processing Unit » (GPU) est un élément crucial pour les *gamers*, car c’est lui qui est 
en charge de calculer les images afin de pouvoir les afficher à l’écran. Celle-ci prend ainsi en charge les images en 2D 
et en 3D que ce soit une page web, une vidéo ou encore une partie endiablée de votre jeu favori. Une carte graphique 
doit donc réaliser un nombre élevé de tâches, puisque qu’elle doit par exemple calculer la couleur à afficher sur 
chaque pixel de l’écran de votre smartphone. Par exemple dans le cas d’une image Full HD (1920×1080), le GPU affiche 
2 073 600 pixels différents ou 8 294 400 pixels pour de l’Ultra HD (3840×2160).

Rappelons également que ce calcul est fait selon la fréquence de rafraichissement de l’écran. Celle-ci peut par 
exemple varier entre 60 et 120 fois par secondes c’est-à-dire entre 60 Hz et 120 Hz.

### La puce neuronale (NPU)
La puce neuronale ou « Neuronal Processing Unit » (NPU) est une puce en charge de l’intelligence artificielle des
smartphones. Les calculs de l’intelligence artificielle ont longtemps été faits par le biais de serveurs dans le cloud 
(distant). Néanmoins, depuis quelques années pour des raisons de rapidité et de respect de la vie privée, les calculs 
se font désormais directement sur les smartphones. C’est utile par exemple dans « Google Translate » pour 
reconnaître des caractères, pour optimiser les photos ou encore l’autonomie.

### Le modem (Interface)
Les smartphones embarquent également dans le SoC une unité réseau assurant la prise en charge des différents 
protocoles de communication. Cette unité est la partie la plus compliquée à développer et à implémenter sur un SoC. 
Néanmoins, il s’agit d’un élément crucial afin d’assurer le nomadisme d’un smartphone en itinérance. Le modem 
intégré au SoC gère non seulement le Wifi, le Bluetooth, le NFC ou bien encore les technologies mobiles. C’est-à-dire 
la 4G, ou plus récemment la 5G mais également de plus vieux réseaux tels que la 3G.

### Le processeur de signal numérique (DSP)
Le processeur de signal numérique ou « Digital Signal Processor » (DSP) est en charge de traiter les signaux
numériques. Ainsi, il va permettre le filtrage, la compression ou encore l’extraction de différents signaux tels que la 
musique ou encore une vidéo.

### Le processeur de signal d’images (ISP)
Le **processeur d’image** ou « Image Signal Processor » (ISP) est une puce prenant en charge la création d’images
numériques. En effet de par leurs tailles minuscules, les capteurs photo des smartphones ne sont pas de très bonne 
qualité d’un point de vue de l’optique pure. La qualité qu’il est actuellement possible d’obtenir va être intimement 
liée à cette puce qui va compenser logiquement certaines limitations optiques (zoom numérique …).

### Le processeur de sécurité (SPU)

Le **processeur de sécurité** ou « Secure Processing Unit » (SPU) est le « bouclier » du smartphone. Son alimentation 
électrique est indépendante afin de ne pas pouvoir être éteint en cas d’attaque sur celui-ci. Le SPU est d’une 
importance capitale. En effet celui-ci va stocker les données biométriques, bancaires, la carte SIM ou encore lestitres
de transport. C’est lui qui contient les clés de chiffrement des données de l’utilisateur.

## Avantages des SoC

Outre leur taille miniaturisée bien adaptée aux terminaux nomades (smartphones et tablettes), les SoC offrent d'autres 
avantages par rapport aux systèmes "classiques" rencontrés dans les ordinateurs :

- les SoC sont conçus pour consommer beaucoup moins d'énergie qu'un système classique à puissance 
équivalente de calculs ;
- cette consommation réduite d’énergie permet dans la plupart des cas de s'affranchir de la présence d’un
système de refroidissement actif comme les ventilateurs ou de type « watercooling » ; un système équipé de 
SoC est donc silencieux car il chauffe relativement peu ;
- étant donné les distances très faibles entre, par exemple, le CPU et la mémoire, les données circulent 
beaucoup plus vites, ce qui permet d'améliorer grandement les performances ; en effet, dans les systèmes 
"classiques" les BUS chargés d’acheminer les données sont souvent des "goulots d'étranglement" en termes 
de performances à cause de la vitesse limitée de circulation des données.

En revanche, le principal inconvénient d’un SoC est que là ou un ordinateur équipé d'une carte mère permet de faire 
évoluer les composants individuellement, l'extrême intégration du SoC présente en revanche l'inconvénient de 
n'autoriser aucune mise à jour possible du matériel.

Le marché des SoC a aujourd'hui un poids économique très important. On commence même à trouver des ordinateurs portables équipés de SoC à la place des cartes mères "classiques". Les SoC commencent doucement, mais sûrement à rattraper les systèmes "classiques" en termes de puissance (pour les systèmes "classiques" d'entrées et de moyennes gammes, mais aussi haut de gamme avec les nouveaux MacBook d'Apple équipé de Soc M3).