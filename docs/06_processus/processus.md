---
author: Jason LAPEYRONNIE
title: Processus
---

Toute machine est dotée d’un système d’exploitation qui a pour fonction de charger les programmes depuis la mémoire de masse et de lancer leur exécution en leur créant des processus (process en anglais), de gérer l’ensemble des ressources, de traiter les interruptions ainsi que les entrées-sorties et enfin d’assurer la sécurité globale du système.

## Notion de processus

Tous les systèmes d'exploitation "modernes" (Linux, Windows, MacOS, Android, iOS...) sont capables de gérer l'exécution de plusieurs processus en même temps. Mais pour être précis, cela n'est pas un réel "en même temps", mais plutôt un "chacun son tour". Pour gérer ce "chacun son tour", les systèmes d'exploitation attributs des "états" au processus afin de les hiérarchiser.

Un processus informatique (en anglais process), est défini par :

- un ensemble d'instructions à exécuter (un programme) ;
- un espace mémoire pour les données de travail ;
- éventuellement, d'autres ressources, comme des descripteurs de fichiers, des ports réseau, etc.

Un ordinateur équipé d'un système d'exploitation à temps partagé est capable d'exécuter plusieurs processus de façon "quasi-simultanée". S'il y a plusieurs processeurs, l'exécution des processus est distribuée de façon équitable entre chacun d’entre eux.


Un processus peut se rencontrer sous différents états.

- Lorsqu’un processus est en train de s'exécuter, c’est-à-dire qu'il utilise les ressources du microprocesseur, on dit qu’il est dans l'état **"élu"**.
- Un processus qui se trouve dans l'état élu peut demander à accéder à une ressource pas forcément disponible instantanément (typiquement lire une donnée sur le disque dur). Le processus ne peut pas poursuivre son exécution tant qu'il n'a pas obtenu cette ressource. En attendant de recevoir cette ressource, il passe de l'état **"élu"** à l'état  **"bloqué"**.
- Lorsque le processus finit par obtenir la ressource attendue, celui-ci peut potentiellement reprendre son exécution. Cependant, bien que les systèmes d'exploitation permettent de gérer plusieurs processus "en même temps", il n’en demeure pas moins qu’un seul processus peut se trouver dans un état "élu" (le microprocesseur ne peut "s'occuper" que d'un seul processus à la fois). Quand un processus passe d'un état "élu" à un état "bloqué", un autre processus peut alors "prendre sa place" et passer dans l'état "élu". Le processus qui vient de recevoir la ressource attendue ne va donc pas forcément pouvoir reprendre son exécution tout de suite, car pendant qu'il était dans à état "bloqué" un autre processus a "pris sa place". Un processus qui quitte l'état bloqué ne repasse pas forcément à l'état "élu", il peut, en attendant que "la place se libère" passer dans l'état **"prêt"** ( "j'ai obtenu ce que j'attendais, je suis prêt à reprendre mon exécution dès que la "place sera libérée"").

![Diagramme d'état](diagramme.png)


Le passage de l'état "prêt" vers l'état "élu" constitue l'opération **"d'élection"**. Le passage de l'état élu vers l'état bloqué
est l'opération de "blocage". Un processus est toujours créé dans l'état "prêt". Pour se terminer, un processus doit 
obligatoirement se trouver dans l'état "élu".


**Il est fondamental de bien comprendre que le "chef d'orchestre" qui attribue aux processus leur état "élu", "bloqué"  ou "prêt" est le système d'exploitation (OS – Operating System). On dit que le système d’exploitation gère l'ordonnancement des processus (un processus sera prioritaire sur un autre...).**

**Remarque** : un processus qui utilise une ressource doit la "libérer" une fois qu'il a fini de l'utiliser afin de la rendre disponible pour les autres processus. Pour libérer une ressource, un processus doit obligatoirement être dans un état  "élu".

## Création d'un processus

Un processus peut créer un ou plusieurs processus à l'aide d'une commande système ("fork" sous les systèmes de type Unix). Imaginons un processus A qui crée un processus B. On dira que A est le père de B et que B est le fils de A. B peut, à son tour créé un processus C (B sera le père de C et C le fils de B). On peut modéliser ces relations père/fils par une structure arborescente

Si un processus est créé à partir d'un autre processus, comment est créé le tout premier processus ?

Sous un système d'exploitation comme Linux, au moment du démarrage de l'ordinateur un tout premier processus (appelé processus 0 ou encore Swapper) est créé à partir de "rien" (il n'est le fils d'aucun processus). Ensuite, ce processus 0 crée un processus souvent appelé "init" ("init" est donc le fils du processus 0). 

À partir de "init", les processus nécessaires au bon fonctionnement du système d’exploitation Linux sont créés (par exemple les processus "crond", "inetd", "getty",...). Puis d'autres processus sont créés à partir des fils de "init"...


![Processus](processus.jpg)

Chaque processus possède un identifiant appelé PID (Process Identification), ce PID est un nombre entier. Le premier processus créé au démarrage du système à pour PID 0, le second 1, le troisième 2... Le système d'exploitation utilise un compteur qui est incrémenté de 1 à chaque création de processus, le système utilise ce compteur pour attribuer les PID aux processus.

Chaque processus possède aussi un PPID (Parent Process Identification). Ce PPID permet de connaitre le processus parent d'un processus (par exemple le processus "init" vu ci-dessus à un PID de 1 et un PPID de 0). À noter que le processus 0 ne possède pas de PPID (c'est le seul dans cette situation)


