---
author: Jason LAPEYRONNIE
title: Epreuve pratique
---

La page suivante présente quelques exercices liés à la récursivité issus de la banque nationale d'épreuves.

## Sujet 7, 2023 : Nombres romains

Le but de cet exercice est d’écrire une fonction récursive `traduire_romain` qui prend en paramètre une chaîne de caractères, non vide, représentant un nombre écrit en chiffres romains et qui renvoie son écriture décimale.

Les chiffres romains considérés sont : I, V, X, L, C, D et M. Ils représentent respectivement les nombres 1, 5, 10, 50, 100, 500, et 1000 en base dix.

On dispose d’un dictionnaire `romains` dont les clés sont les caractères apparaissant dans l’écriture en chiffres romains et les valeurs sont les nombres entiers associés en écriture décimale :

```python
romains = {"I":1, "V":5, "X":10, "L":50, "C":100, "D":500, "M":1000}
```

Le code de la fonction `traduire_romain` repose sur le principe suivant :

- la valeur d’un caractère est ajoutée à la valeur du reste de la chaîne si ce caractère a une valeur supérieure (ou égale) à celle du caractère qui le suit ;
- la valeur d’un caractère est retranchée à la valeur du reste de la chaîne si ce 
caractère a une valeur strictement inférieure à celle du caractère qui le suit.

Ainsi, XIV correspond au nombre 10 + 5 - 1 puisque :

- la valeur de X (10) est supérieure à celle de I (1), on ajoute donc 10 à la valeur du 
reste de la chaîne, c’est-à-dire IV ;
- la valeur de I (1) est strictement inférieure à celle de V (5), on soustrait donc 1 à 
la valeur du reste de la chaîne, c’est-à-dire V.

On rappelle que pour priver une chaîne de caractères de son premier caractère, on 
utilisera l’instruction : `nom_de_variable[1:]`

Par exemple, si la variable mot contient la chaîne "CDI", mot[1:] renvoie "DI".

Compléter le code de la fonction traduire_romain et le tester

???+ question "Programmez !"

    {{IDE('scripts/romain')}}

## Sujet 9, 2023 : recherche dichotomique

Soit `tab` un tableau non vide d'entiers triés dans l'ordre croissant et `n` un entier.

La fonction `chercher` ci-dessous doit renvoyer un indice où la valeur `n` apparaît dans
`tab` si cette valeur y figure et `None` sinon.

Les paramètres de la fonction sont :

- `tab`, le tableau dans lequel s'effectue la recherche ;
- `n`, l'entier à chercher dans le tableau ;
- `i`, l'indice de début de la partie du tableau où s'effectue la recherche ;
- `j`, l'indice de fin de la partie du tableau où s'effectue la recherche.


???+ question "Programmez !"

    L’algorithme demandé est une recherche dichotomique récursive.  
    Recopier et compléter le code de la fonction `chercher` suivante :

    {{IDE('scripts/sujet09_exo2')}}

## Sujet 45, 2023 : conversion binaire-décimal

L’objectif de cet exercice est d’écrire deux fonctions récursives `dec_to_bin` et `bin_to_dec` assurant respectivement la conversion de l’écriture décimale d’un nombre entier vers son écriture en binaire et, réciproquement, la conversion de l’écriture en binaire d’un nombre vers son écriture décimale.

Dans cet exercice, on s’interdit l’usage des fonctions Python `bin` et `int`.

On rappelle sur l’exemple ci-dessous une façon d’obtenir l’écriture en binaire du 
nombre 25 :

$\begin{eqnarray*}
25 &=& 1+2 \times 12 \\
&=& 1+2(0+2 \times 6) \\
&=& 1+2(0+2(0+2\times 3))\\
&=& 1+2(0+2(0+2 \times (1+2 \times 1)))\\
&=& 1 \times 2^0 + 0 \times 2^1 + 0 \times 2^2 + 1 \times 2^3 + 1 \times 2^4
\end{eqnarray*}$


L’écriture en binaire de 25 est donc 11001. 0n rappelle également que : 

- `a // 2` renvoie le quotient de la division euclidienne de a par 2.
- `a % 2` renvoie le reste dans la division euclidienne de a par 2.

On indique enfin qu’en Python si mot = "informatique" alors :

- `mot[-1]` renvoie 'e', c’est-à-dire le dernier caractère de la chaîne de caractères mot.
- `mot[:-1]` renvoie 'informatiqu' , c’est-à-dire l’ensemble de la chaîne de caractères mot privée de son dernier caractère.

Compléter, puis tester, les codes de ces deux fonctions.

On précise que la fonction récursive `dec_to_bin` prend en paramètre un nombre entier et renvoie une chaîne de caractères contenant l’écriture en binaire du nombre passé en paramètre. Exemple :

```python
>>> dec_to_bin(25)
'11001'
```

La fonction récursive `bin_to_dec` prend en paramètre une chaîne de caractères représentant l’écriture d’un nombre en binaire et renvoie l’écriture décimale de ce nombre.

```python
>>> bin_to_dec('101010')
42
```

???+ question "Programmez !"

    {{IDE('scripts/dectobin')}}

???+ question "Programmez !"

    {{IDE('scripts/bintodec')}}


