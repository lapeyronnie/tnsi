def raccourcir(ch):
    return ch[1:-1]

def regleB(chaine):
    n = len(chaine)
    if n >= 2:
        return chaine[0] == "b" and chaine[n-1] == "b" and (regleA(raccourcir(chaine)) or regleB(raccourcir(chaine)))
    else:
        return False