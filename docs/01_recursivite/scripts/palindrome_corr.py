def est_palindrome(chaine) :
    n = len(chaine)
    for i in range(n):
        if chaine[i] != chaine[n-1-i]:
            return False
    return True

#Il est aussi possible de se limiter à parcourir la moitié du mot