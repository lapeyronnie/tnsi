def puissance(x : float, n : int):
    if n == 0 :
        return 1
    else :
        return x * puissance(x, n-1)