def raccourcir(ch):
    return ch[1:-1]

def regleA(chaine):
    n = len(chaine)
    if n >= 2:
        return chaine[0] == "a" and chaine[n-1] == "a" and regleA(raccourcir(chaine))
    else:
        return chaine == "a"