def dec_to_bin (nb_dec):
    q, r = nb_dec // 2, nb_dec % 2
    if q == 0 :
        return str(r)
    else:
        return dec_to_bin(q) + str(r)