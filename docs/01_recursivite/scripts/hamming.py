def hamming(ch1, ch2) :
    # si la chaîne de caractère ch1 est vide, on renvoie 0
    # sinon, si le premier caractère de ch1 est différent du premier caractère de ch2,
    # on renvoie 1 + la distance de hamming entre ch1[1:] et ch2[1:]
    # sinon, on renvoie la distance de hamming entre ch1[1:] et ch2[2:]