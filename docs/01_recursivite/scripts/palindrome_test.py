assert est_palindrome('kayak') == True
assert est_palindrome('kayake') == False
assert est_palindrome('') == True
assert est_palindrome('bonjourruojnob') == True