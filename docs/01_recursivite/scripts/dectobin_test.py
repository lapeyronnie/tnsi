assert dec_to_bin(25) == '11001'
assert dec_to_bin(33) == '100001'
assert dec_to_bin(121) == '1111001'