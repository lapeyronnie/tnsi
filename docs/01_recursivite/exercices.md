---
author: Jason LAPEYRONNIE
title: Exercices
---

## Centres étrangers 2022, sujet 2 (extrait)

Voici une fonction codée en Python

```python
def f(n):
    if n == 0:
        print("Partez !")
    else:
        print(n)
        f(n-1)
```

1. Qu'affiche la commande `f(5)` ?
2. Pourquoi dit-on de cette fonction qu'elle est récursive ?

??? success "Solution"

    La commande `f(5)` affiche
    ```python
    5
    4
    3
    2
    1
    Partez !
    ```
    Il s'agit d'une fonction récursive car la fonction s'appelle elle-même.

## Inversion d'une chaîne de caractères

On souhaite programmer une fonction récursive `inverser` qui prend en entrée une chaîne de caractères `ch` et qui renvoie une chaîne de caractères qui est la chaîne `ch`écrite dans l'autre sens.

On rappelle que si `ch` est une chaîne de caractère, `ch[0]` est son premier caractère, `ch[1]` est son deuxième caractère, etc. Par ailleurs, `ch[1:]` renvoie toute la chaîne de caractère `ch` privée de son premier caractère.

1. Quelle doit être la chaîne renvoyée si la chaîne entrée est la chaîne vide ?
2. Parmi les choix suivants, lequel correspond à l'appel récursif correspondant à la fonction `inverser` ?

```python
# Choix numéro 1
return inverser(ch[1:])
# Choix numéro 2
return inverser(ch[1:]) + ch[0]
# Choix numéro 3
return ch[1:] + inverser(ch[0])
# Choix numéro 4
return ch[0] + inverser(ch[1:])
```

??? success "Solution"

    Si la chaîne entrée est vide, la chaîne renvoyée est également la chaîne vide.
    
    Le bon choix est le choix numéro 2 : le premier caractère de la chaîne est placée à la fin. Devant, on met alors la chaîne inversée à partir de son deuxième caractère.

3. Implémenter la fonction `inverser` dans l'encart ci-dessous.

???+ question "Programmez !"

    {{IDE('scripts/inverser')}}

## Longueur d'une chaîne de caractères

Compléter la fonction récursive `longueur` suivante, qui prend en entrée une chaîne de caractères `ch` et qui renvoie la logueur de la chaîne. Il est évidemment interdit d'utiliser la fonction `len`. Pour cela,

- la condition d'arrêt correspondra à une chaîne vide
- l'appel récusrif consistera à ajouter 1 à la longueur de la chaîne `ch` privée de son premier caractère. On rappelle que cette chaîne s'écrit `ch[1:]`

???+ question "Programmez !"

    {{IDE('scripts/longueur')}}

## Liban 2023, Sujet 1

Un palindrome est un mot qui se lit de la même manière de la gauche vers la droite que de la droite vers la gauche (exemple : « kayak » est un palindrome).

On propose ci-dessous une fonction pour tester si un mot est un palindrome. On précise que, pour une chaîne de caractères `chaine` :

- l’instruction `len(chaine)` renvoie sa longueur ;
- l’instruction `chaine[-1]` renvoie son dernier caractère ;
- l’instruction `chaine[1:-1]` renvoie la chaîne privée de son premier caractère et de son
dernier caractère.

```python
def tester_palindrome(chaine) :
    if len(chaine) < 2 :
        return True
    elif chaine[0] != chaine[-1] :
        return False
    else :
        return tester_palindrome(chaine[1:-1])
```


1 - On saisit, dans la console, l’instruction suivante : `tester_palindrome('kayak')`. Combien de fois est appelée la fonction tester_palindrome lors de l’exécution de cette instruction ? On veillera à compter l’appel initial.

??? success "Solution"

    Un premier appel est effectué sur la chaîne 'kayak', un deuxième sur la chaîne 'aya', puis un troisième sur la chaîne 'y'. La longueur de la chaîne étant alors strictement inférieur à 2, il n'y a pas de nouvel appel récursif, la condition d'arrêt est atteinte.

2 - a. Justifier que la fonction `tester_palindrome` est récursive.  
    b. Expliquer pourquoi l’appel à la fonction `tester_palindrome` se terminera quelle que soit la chaîne de caractères sur laquelle elle s’applique.

??? success "Solution"

    La fonction `tester_palindrome` s'appelle elle-même, elle est donc récursive.

    Entre chaque appel, la longueur de la chaîne entrée dans la fonction est diminuée de 2. Il y aura donc bien un moment où cette chaîne sera de longueur strictement inférieure à 2 (à moins que cette chaîne ne soit extrêmement longue et que la limite de la taille de la pile d'appels récursifs ne soit atteinte !)

3 - La saisie, dans la console, de l’instruction `tester_palindrome(53235)` génère une erreur.

a. Parmi les quatre propositions suivantes, indiquer le type d’erreur affiché :

- ZeroDivisionError
- ValueError
- TypeError
- IndexError

b. Proposer une ou plusieurs instructions qu’on pourrait écrire entre la ligne 1 et la ligne 2 du code de la fonction `tester_palindrome` et permettant d’afficher clairement cette erreur à l’utilisateur.

??? success "Solution"

    La deuxième ligne essaye de calculer la longueur de la chaîne de caractères donnée en entrée. Or, dans ce cas, c'est un entier qui est donnée en entrée : il ne s'agit pas d'une variable du bon type, la fonction affiche donc TypeError

    Pour prévenir cela, il est poassible d'ajouter la ligne `assert type(chaine) is str, "Une chaine de caractère est attendue en entrée"`

4 - Écrire le code d’une fonction **itérative** (non récursive) `est_palindrome` qui prend en paramètre une chaîne de caractères et renvoie un booléen égal à True si la chaîne de caractères est un palindrome, False sinon.

???+ question "Programmez !"

    {{IDE('scripts/palindrome')}}

## Somme des valeurs d'une chaîne

On considère la programme suivant, utilisé pour calculer la somme des valeurs d'une liste composée d'entiers.

```python
def somme(L):
    if L == '':
        return ...
    else :
        return L[0] + somme(L[1:])
```

1. Compléter le programme précédent
2. Pourquoi peut-on dire que cette fonction est récursive ?
3. Construire l'arbre des appels si l'on exécute la commande `somme([1,5,9])`
4. Écrire une version itérative de ce programme

???+ question "Programmez !"

    {{IDE('scripts/sommeliste')}}

## Distance de Hamming

On considère deux chaînes de caractères de même longueur. On appelle distance de Hamming entre ces deux chaînes le nombre de caractères qui diffèrent entre ces deux chaînes en étant à la même position. Par exemple, la distance de Hamming entre les chaînes 'RAMER' et 'RALER' est 1 (le M est différent du L). La distance de Hamming entre les chaînes 'RAMER' et 'CASES' vaut 3.

Compléter la fonction récursive suivante pour qu'elle renvoie la distance de Hamming entre deux chaînes de caractères.

On rappelle que si `ch` est une chaîne de caractère, `ch[0]` est son premier caractère, `ch[1]` est son deuxième caractère, etc. Par ailleurs, `ch[1:]` renvoie toute la chaîne de caractère `ch` privée de son premier caractère.

???+ question "Programmez !"

    {{IDE('scripts/hamming')}}

## Une fonction mystère

On considère la fonction suivante, écrite en Python, et qui prend en entrée deux chaînes de caractères de même longueur `ch1`et `ch2`.

```python
def mystere(ch1, ch2):
    if ch1 == '':
        return ''
    else :
        return ch1[0] + ch2[0] + mystere(ch1[1:],ch2[1:])
```

1. Cette fonction est-elle récursive ?
2. Que renverra cette fonction si l'on effectue la commande `mystere('salut','barbe')`. Plus généralement, que cait cette fonction ?
3. Proposer une version itérative de cette fonction

???+ question "Programmez !"

    {{IDE('scripts/mystere')}}

## Plus long préfixe commun

On considère le problème suivant : on a à disposition deux listes d'entiers et on souhaite déterminer le plus long préfixe commun, c'est-à-dire la plus longue sous-liste commune à ces deux listes et qui commence par les mêmes nombres.

Par exemple, le plus long préfixe commun aux listes `[1, 2, 4, 5, 7]` et `[1, 2, 4, 8, 1, 3]` est `[1, 2, 4]`.

On propose la fonction suivante

```python
def prefixe(L1,L2):
    pref = []
    m = max(len(L1), len(L2))
    for i in range(m):
        if L1[i] == L2[i]:
            pref.append(L1[i])
        else :
            return pref
    return pref
```

1. Cette fonction est-elle récursive ?
2. Quelle est l'utilité du dernier `return pref` de cette fonction ?
3. Proposez une version récursive de ce programme



## Polynésie 2022, Sujet 1

On rappelle qu'une chaîne de caractères peut être représentée en Python par un texte
entre guillemets "" et que :

-  la fonction `len` renvoie la longueur de la chaîne de caractères passée en
paramètre ;
- si une variable `ch` désigne une chaîne de caractères, alors `ch[0]` renvoie son
premier caractère, `ch[1]` le deuxième, etc. ;
- l'opérateur + permet de concaténer deux chaînes de caractères.

```python
>>> texte = "bricot"
>>> len(texte)
6
>>> texte[0]
"b"
>>> texte[1]
"r"
>>> "a" + texte
"abricot"
```

On s'intéresse dans cet exercice à la construction de chaînes de caractères suivant
certaines règles de construction.

**Règle A** : une chaîne est construite suivant la règle A dans les deux cas suivants:

- soit elle est égale à "a" ;
- soit elle est de la forme "a"+chaine+"a", où chaine est une chaîne de
caractères construite suivant la règle A.

**Règle B** : une chaîne est construite suivant la règle B dans les deux cas suivants :

- soit elle est de la forme "b"+chaine+"b", où chaine est une chaîne de
caractères construite suivant la règle A ;
- soit elle est de la forme "b"+chaine+"b", où chaine est une chaîne de
caractères construite suivant la règle B.

On a reproduit ci-dessous l'aide de la fonction choice du module random.

```python
>>> from random import choice
>>> help(choice)
Help on method choice in module random:
choice(seq) method of random.Random instance
Choose a random element from a non-empty sequence.
```

La `fonction A()` ci-dessous renvoie une chaîne de caractères construite suivant la règle
A, en choisissant aléatoirement entre les deux cas de figure de cette règle.

```python
def A():
    if choice([True, False]):
        return "a"
    else:
        return "a" + A() + "a"
```

1 - a. Cette fonction est-elle récursive ? Justifier.  
    b. La fonction `choice([True, False])` peut renvoyer False un très grand
nombre de fois consécutives. Expliquer pourquoi ce cas de figure amènerait à
une erreur d'exécution.

??? success "Solution"

    Cette fonction est récursive puisque qu'elle fait appel à elle-même.

    On pourrait avoir une erreur d'exécution si le nombre d'appels récursifs était trop nombreux : on dépasserait alors le taille de la pile.

Dans la suite, on considère une deuxième version de la fonction A. À présent, la fonction
prend en paramètre un entier n tel que, si la valeur de n est négative ou nulle, la
fonction renvoie "a". Si la valeur de n est strictement positive, elle renvoie une chaîne
de caractères construite suivant la règle A avec un n décrémenté de 1, en choisissant
aléatoirement entre les deux cas de figure de cette règle.

???+ question "Programmez !"

    {{IDE('scripts/exo_poly_22')}}

2 - a. Compléter aux emplacements des points de
suspension ... le code de cette nouvelle fonction A.  
    b. Justifier le fait qu'un appel de la forme `A(n)` avec n un nombre entier positif
inférieur à 50, termine toujours.

??? success "Solution"

    Si `choice[True, False]` renvoie True avant le 50ème appel récursif, alors la fonction s'arrêtera à cet instant. En revanche, si celle-ci ne renvoie que des False durant les 50 premiers appels, alors à cet instant, la valeur de n passera à 0. La condition `n<=0 or choice[True, False]` sera alors vraie, et la fonction renverra alors `a`.

On donne ci-après le code de la fonction récursive B qui prend en paramètre un entier n
et qui renvoie une chaîne de caractères construite suivant la règle B.

```python
def B(n):
    if n <= 0 or choice([True, False]):
        return "b" + A(n-1) + "b"
    else:
        return "b" + B(n-1) + "b"
```

On admet que :

- les appels A(-1)et A(0) renvoient la chaîne "a";
- l’appel A(1) renvoie la chaîne "a" ou la chaîne "aaa";
- l’appel A(2) renvoie la chaîne "a", la chaîne "aaa" ou la chaîne "aaaaa".

3 - Donner toutes les chaînes possibles renvoyées par les appels B(0), B(1)
et B(2).

??? succes "Solution"

    - B(0) renvoie systématique bab
    - B(1) renvoie bab ou bbabb
    - B(2) renvoie bab, baaab, bbabb ou bbbabbb

On suppose maintenant qu'on dispose d'une fonction raccourcir qui prend comme
paramètre une chaîne de caractères de longueur supérieure ou égale à 2, et renvoie la
chaîne de caractères obtenue à partir de la chaîne initiale en lui ôtant le premier et le
dernier caractère.

Par exemple :
```python
>>> raccourcir("abricot")
"brico"
>>> raccourcir("ab")
""
```

4 -  a. Compléter les points de suspension ... du code de la
fonction `regleA` ci-dessous pour qu'elle renvoie True si la chaîne passée en
paramètre est construite suivant la règle A, et False sinon.
???+ question "Programmez !"

    {{IDE('scripts/exo_poly_reglea')}}

    b. Écrire le code d’une fonction regleB, prenant en paramètre une chaîne de
caractères et renvoyant True si la chaîne est construite suivant la règle B,
et False sinon.

???+ question "Programmez !"

    {{IDE('scripts/exo_poly_regleb')}}