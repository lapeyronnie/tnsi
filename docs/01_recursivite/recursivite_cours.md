---
author: Jason LAPEYRONNIE
title: Cours - Récursivité
---

## Exemple introductif

Vous avez vu l'année dernière comment implémenter des fonctions en Python. Pour rappel, cela se fait notamment à l'aide du mot clé **def**. La fonction suivante permet par exemple d'afficher un compte à rebours allant d'un entier n donné en entrée.

```python
def rebours(n):
    cpt = n
    while cpt > -1 :
        print(cpt)
        cpt = cpt - 1
    print("Décollage !")
```

L'appel `rebours(5)` produit alors la sortie suivante

```
5
4
3
2
1
0
Décollage !
```

Il est alors possible d'aborder cet exemple avec un autre point de vue. Pour établir le compte à rebours de 5 à 0, il suffit...

- d'afficher le nombre 5
- d'afficher le compte à rebours en partant du nombre 4

Evidemment, le raisonnement que nous avons fait ici peut être repris pour établir le compte à rebours en partant de 4, il suffit alors...

- d'afficher le nombre 4
- d'afficher le compte à rebours en partant du nombre 3

Et ainsi de suite, jusqu'à ce que le compteur atteigne finalement -1 et que l'on doive afficher la chaîne de caractère "Décollage !". Tout ceci nous permet alors d'aboutir à une nouvelle implémentation de notre compte à rebours.

```python
def rebours_v2(n : int):
    #Si le nombre n vaut -1, on affiche Décollage !
    if n <= -1 :
        print("Décollage !")
    #Sinon, on affiche le nombre n puis on effectue le compte à rebours en partant de n-1
    else :
        print(n)
        rebours_v2(n-1)
```

Les deux premières lignes sont essentielles pour s'assurer que notre programme arrive bien à son terme ! C'est ce que l'on appelle la **condition d'arrêt** du programme. On remarque ensuite que la fonction `rebours_v2` fait appel à elle-même : on dit que cette fonction est récursive.

## Fonctions récursives

### Définition

!!! abstract "Fonctions récursives"

    Une *fonction récursive* est une fonction qui s'appelle elle-même jusqu'à aboutir à un cas où la résolution du problème est directe, que l'on appelle la *condition d'arrêt* de la fonction (on trouve également la terminologie *cas de base*, *situation de terminaison*, *cas d'arrêt*...).

    Lorsque l'on implémente une fonction récursive, sa structure ressemble très souvent au schéma suivant
    
    ```python
    def fonction_recursive(n) :
        if condition_d_arret :
            return solution
        else :
            fonction_recursive(nombre_inferieur_a_n)
    ```


!!! info "Récursif et récurrence..."

    La notion de *fonction récursive* est évidemment à rapprocher de celle de *suite récurrente* que vous avez rencontrée si vous avec suivi la spécialité Mathématiques en classe de Première. 

    Pour ces types particuliers de suite, on définissait le premier terme $u_0$ ainsi qu'une relation de récurrence qui permettait de passer d'un terme au suivant. 

    Par exemple, considérons la suite $(u_n)$ définie par $u_0=5$ et, pour tout entier naturel $n$, $u_{n+1} = 3u_n +2$. Les termes de la suite $(u_n)$ peuvent alors être calculés par la fonction suivante.

    ```python
    def u(n):
        if n == 0:
            return 5
        else : 
            return 3 * u(n-1) + 2
    ```

    L'instruction `u(5)` permettra alors de renvoyer le 5ème terme de la suite $(u_n)$.

!!! danger "Attention"

    Il ne faut **surtout pas** oublier la condition d'arrêt dans l'implémentation de votre algorithme récursif. Sans cela, votre fonction continuera indéfiniment à faire des appels récursifs et ne s'arrêtera qu'une fois la sommet de la pile d'appels atteint (voir plus tard)

![Recursivité](memerecursivite.webp)

### Quelques exemples classiques

Quand peut-on alors penser à un algorithme récursif ? Il faut que la résolution du problème avec une entrée d'une certaine taille passe par la résolution du même problème avec une entrée de taille inférieure, jusqu'à atteindre un minimum pour lequel la solution est alors simple à obtenir. 

Un exemple très simple est l'implémentation de la factorielle. En mathématiques, la factorielle d'un entier $n$, que l'on note $n!$, est le produit de touts les entiers de 1 à $n$. Par exemple, $5! = 1 \times 2 \times 3  \times 4 \times 5 = 120$. On convient par ailleurs que $0!=1$.

Cette factorielle peut alors s'implémenter en utilisant un algorithme itératif (qui utilise une boucle **for**).

```python
def factorielle_iteratif(n : int):
    produit = 1 
    for i in range(1, n + 1) :
        produit = produit * i
    return produit
```

Réfléchissons autrement : combien vaut alors $6!$ ? On pourrait bien entendu reprendre le calcul depuis le début, mais tâchons d'être plus futé ! On a en effet $6! = 1 \times 2 \times 3  \times 4 \times 5 \times 6$, mais nous avons déjà calculé la première partie de ce produit. En fait, on a $6! = 5! \times 6$. Pour calculer la factorielle de 6, il faut déjà être capable de calculer la factorielle de 5. On voit ici la récursivité pointer le bout de son nez.

L'algorithme du calcul de la factorielle d'un nombre entier $n$ peut alors se construire ainsi :

- si ce nombre $n$ vaut 0, sa factorielle vaut 1 : c'est notre cas de base, que nous pouvons résoudre instantanément. La condition d'arrêt est ici `n==0`.
- sinon, on multiplie $n$ par la factorielle de $n-1$.

Ce qui donne l'implémentation suivante en Python...

```python
def factorielle(n :int) :
    if n == 0 :
        return 1
    else :
        return n * factorielle(n-1)
```

???+ question "Exercice"

    Ce que nous faisons pour le produit des premiers entiers peut alors être utilisé pour calculer la somme des premiers entiers...

    Constuire une fonction récursive `somme` qui prend en entrée un entier $n$ et qui renvoie la somme de tous les entiers jusqu'à $n$, c'est-à-dire $1+2+3+4+...+n$.


    {{ IDE('scripts/somme') }}



Autre algorithme très classique que l'on peut programmer par récurrence : la puissance entière d'un nombre. On rappelle que si l'on se donne un réel $x$ et un entier naturel $n$, le nombre $x^n$ est égal au produit $x \times x \times \dots \times x$ où le réel $x$ apparaît $n$ fois. On a alors $x^n = x \times x^{n-1}$ : là encore, calculer la puissance $n$ passe par le calcul de la puissance précédente. On convient par ailleurs que $x^0=1$.

???+ question "Exercice"

    Implémenter la fonction récursive `puissance` qui prend en entier un flottant $x$ et un entier naturel $n$ et qui renvoie la valeur de $x^n$.


    {{ IDE('scripts/puissance') }}

## Arbre et piles d'appels

### Appel d'une autre fonction

Observons les deux fonctions suivantes

```python
def fonction_a():
    print("\ndebut de la fonction A")
    for i in range(5):
        print(f"A{i}", end=" ")
    print("\nfin de la fonction A")

def fonction_b():
    print("debut de la fonction B")
    for i in range(5):
        print(f"B{i}", end=" ")
        if i==2:
            fonction_a()
    print("\nfin de la fonction B")
```

Lors de l'exécution de la `fonction_a`, la sortie suivante s'affiche

```
debut de la fonction A
A0 A1 A2 A3 A4 
fin de la fonction A
```

On observe dans la `fonction_b` un appel à la `fonction_a`. Par ailleurs, ces fonctions semblent partager un compteur commun... Qu'en est-il vraiment ? L'exécution de la `fonction_b` va répondre à nos interrogations. La sortie affichée est la suivante.

```
debut de la fonction B
B0 B1 B2 
debut de la fonction A
A0 A1 A2 A3 A4 
fin de la fonction A
B3 B4 
fin de la fonction B
```

On observe au milieu de ce résultat la sortie qui correspond à l'exécution entière de la `fonction_a`. En fait, lorsque le compteur `i` dans la `fonction_b` atteint la valeur 2, la `fonction_a` est exécutée. La `fonction_b` interrompt alors son exécution le temps que celle de la `fonction_a` arrive à son terme. 

Une fois la `fonction_a` terminée, la `fonction_b` reprend alors au moment où elle s'était interrompu. 

Ce comportement peut être visualisé à [l'adresse suivante](https://pythontutor.com/visualize.html#code=def%20fonction_a%28%29%3A%0A%20%20%20%20print%28%22%5Cndebut%20de%20la%20fonction%20A%22%29%0A%20%20%20%20for%20i%20in%20range%285%29%3A%0A%20%20%20%20%20%20%20%20print%28f%22A%7Bi%7D%22,%20end%3D%22%20%22%29%0A%20%20%20%20print%28%22%5Cnfin%20de%20la%20fonction%20A%22%29%0A%0Adef%20fonction_b%28%29%3A%0A%20%20%20%20print%28%22debut%20de%20la%20fonction%20B%22%29%0A%20%20%20%20for%20i%20in%20range%285%29%3A%0A%20%20%20%20%20%20%20%20print%28f%22B%7Bi%7D%22,%20end%3D%22%20%22%29%0A%20%20%20%20%20%20%20%20if%20i%3D%3D2%3A%0A%20%20%20%20%20%20%20%20%20%20%20%20fonction_a%28%29%0A%20%20%20%20print%28%22%5Cnfin%20de%20la%20fonction%20B%22%29%0A%20%20%20%20%0Afonction_b%28%29&cumulative=false&heapPrimitives=nevernest&mode=edit&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false).

Les différents appels des fonctions s'organisent suivant ce que l'on appelle une **pile d'exécution**. Lorsqu'une nouvelle fonction est apellée, celle-ci est empilée sur la précédente et est exécutée. Les autres fonctions sont mises en pause. Une fois cette deuxième fonction terminée, cette dernière est alors dépilée et la première fonction peut alors reprendre son exécution.



### Appels récursifs

Le fonctionnement n'est pas différent lorsque l'on appelle une fonction récursive : lorsque cette fonction s'appelle elle-même, la première exécution se met en attente, le temps que la deuxième se termine. Cette deuxième exéction peut alors elle-même se mettre en attente pour laisser place à un troisième appel récursif, un quatrième, et ainsi de suite.

Reprenons l'exemple de la factorielle, étudiée un peu plus tôt.

```python
def factorielle(n :int) :
    if n == 0 :
        return 1
    else :
        return n * factorielle(n-1)
```

Déroulons alors ce qui se produit lorsque nous exécutons `fact(4)` :

- puisque 4 est différent de 0, nous sommes dans le deuxième cas de cette fonction. `fact(4)` se met en pause et `fact(3)`est exécuté.
- puisque 3 est différent de 0, `fact(3)` se met en pause et `fact(2)`est exécuté.
- puisque 2 est différent de 0, `fact(2)` se met en pause et `fact(1)`est exécuté.
- puisque 1 est différent de 0, `fact(1)` se met en pause et `fact(0)`est exécuté.
- `fact(0)` renvoie alors la valeur 1

La fonction va alors pouvoir remonter les appels

- `fact(1)` peut reprendre et renvoie `1 * fact(0)`, c'est-à-dire 1
- `fact(2)` peut reprendre et renvoie `2 * fact(1)`, c'est-à-dire 2
- `fact(3)` peut reprendre et renvoie `3 * fact(2)`, c'est-à-dire 6
- `fact(4)` peut reprendre et renvoie `4 * fact(3)`, c'est-à-dire 24

Les différents état de la pile d'exécution sont alors les suivants. Ceux-ci peuvent également être visualisés sur [PythonTutor](https://pythontutor.com/visualize.html#code=def%20fact%28n%29%3A%0A%20%20%20%20if%20n%20%3C%3D%201%3A%0A%20%20%20%20%20%20%20%20return%201%0A%20%20%20%20else%3A%0A%20%20%20%20%20%20%20%20return%20n%20*%20fact%28n%20-%201%29%0A%0Afact%284%29&cumulative=false&heapPrimitives=false&mode=edit&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false)

```mermaid
flowchart LR
    step1["factorielle(4)"]
    step2["factorielle(3) 
    factorielle(4)"]
    step3["factorielle(2)
    factorielle(3)
    factorielle(4)"]
    step4["factorielle(1)
    factorielle(2)
    factorielle(3)
    factorielle(4)"]
    step5["factorielle(0) = 1
    factorielle(1)
    factorielle(2)
    factorielle(3)
    factorielle(4)"]
    step6["factorielle(1) = 1
    factorielle(2)
    factorielle(3)
    factorielle(4)"]
    step7["factorielle(2) = 2
    factorielle(3)
    factorielle(4)"]
    step8["factorielle(3) = 6
    factorielle(4)"]
    step9["factorielle(4) = 24"]
    step1 -->|empile| step2 -->|empile| step3 -->|empile| step4 -->|empile| step5 -->|dépile| step6 -->|dépile| step7 -->|dépile| step8 -->|dépile| step9
```

Une autre représentation possible est celle d'un graphe, appelé **arbre d'appel**.

```mermaid
flowchart LR
    A["factorielle(4)"] -->|Appel| B("factorielle(3)")
    B -->|Appel| C("factorielle(2)")
    C -->|Appel| D("factorielle(1)")
    D -->|Appel| E("factorielle(0)")
    E -->|Renvoie 1| D
    D -->|Renvoie 1| C
    C -->|Renvoie 2| B
    B -->|Renvoie 6| A
    A -->|Renvoie 24| F[Sortie]
```

Toutefois, cette liste d'appels ne peut s'étendre à l'infini : il existe une limite au nombre d'appels dans la pile d'exécution en Python, qu'il est possible de voir en entrant par exemple les commandes suivantes dans la console.

```python
>>> import sys
>>> sys.getrecursionlimit()
3000
```

Ceci signifie qu'il n'est pas possible d'empiler plus de 3000 appels. Au 3001ème, Python affichera l'erreur suivante.

```python
RecursionError: maximum recursion depth exceeded while calling a Python object
```

Pour modifier le nombre maximum d'appels récursifs simultanés, il est possible de procéder comme suit.

```python
>>> import sys
>>> sys.setrecursionlimit(100)
```

## Une autre limite de la récursivité

Bien que les algorithmes récursifs puissent parfois sembler plus naturels et compacts, ils sont également plus gourmands en mémoire. Une autre difficulté de ce type d'algorithme peut se trouver dans la multiplication des appels récursifs (et donc un besoin accru en mémoire et un temps d'exécution bien plus grand), comme dans cet exemple.

La suite de Fibonacci est définie ainsi : ses deux premiers termes valent 1, puis chaque terme de la suite est égal à la somme des deux précédents. Les premiers termes de cette suite sont ainsi 1, 1, 2, 3, 5, 8, 13, 21...

Il est alors possible d'implémenter une fonction récursive qui permet de calculer le terme n de la suite de Fibonacci.

```python
def fibonacci(n) :
    if n == 0 or n == 1 :
        return 1
    else :
        return fibonacci(n-1) + fibonacci(n-2)
```

Nous avons là une récursivité multiple : la fonction fibonacci est appelée plusieurs fois dans une même exécution. Seulement, en excutant ce programme pour de grandes valeurs de n, nous pouvons constater que le temps d'exécution de la fonction nous semble anormalement long. Une explication peut alors être trouvée en dessinant l'arbre d'appel de cette fonction. Prenons par exemple le calcul de fibonacci(5)

```mermaid
flowchart TB
    A["fibonacci(5)"]
    B["fibonacci(4)"]
    C["fibonacci(3)"]
    A --> B
    A --> C
    D["fibonacci(3)"]
    E["fibonacci(2)"]
    B --> D
    B --> E
    F["fibonacci(2)"]
    G["fibonacci(1)"]
    C --> F
    C --> G
    H["fibonacci(2)"]
    I["fibonacci(1)"]
    D --> H
    D --> I
    J["fibonacci(1)"]
    K["fibonacci(0)"]
    H --> J
    H --> K
    L["fibonacci(1)"]
    M["fibonacci(0)"]
    F --> L
    F --> M
    N["fibonacci(1)"]
    O["fibonacci(0)"]
    E --> N
    E --> O
```

Nous pouvons remarquer que certaines valeurs sont calculées plusieurs fois : l'ordinateur ne garde pas en mémoire les valeurs de la suite de Fibonacci qu'il a déjà calculé. Ce problème pourra être résolu lorsque nous aborderons bien plus tard dans l'année la programmation dynamique.