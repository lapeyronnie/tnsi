---
author: Jason LAPEYRONNIE
title: Interface et implémentation
---

Parmi les types de données que vous avez rencontré en classe de Première, on peut distinguer les **types simples** (entier, booléen, flottant) et d'autres types, plus complexes, qu'étaient les **types construits** (tableaux, tuples, dictionnaires).

Chaque type de données avait ses propriétés et ses utilisations : par exemple, le tuple est une séquence **itérable** - c'est-à-dire que l'on peut utiliser une boucle for pour parcrourir ses éléments - mais **non mutable** - ce qui signifie qu'il n'est pas possible de modifier les données à l'intérieur de ce tuple. Par ailleurs, il est possible d'accéder à n'importe quel élément du tuple à partir de sa position. 

Lorsque l'on aborde un problème en informatique, il faut s'interroger sur la structure de données à utiliser. Pour cela, il est indispensable de savoir ce que tel type de données permet ou ne permet pas. Cette compréhension nous permet alors d'établir des algorithmes sans se soucier de la manière dont on écrira le programme ou code correspondant. Cette phase, l'**implémentation**, arrive dans un second temps.

De la même manière, les types de données que vous nous utilisons ont été considérés de manière abstraite : avant d'implémenter les différents types de données dans un langage particulier, ceux-ci ont été spécifiés. Leur structure est ainsi la même, indépendamment du langage dans lequel on souhaite programmer.

## Type abstrait de données

!!! abstract "Structure de données"

    Une **structure de données** est une manière de "ranger" des données dans la mémoire de l'ordinateur afin de les traiter plus facilement.

Selon le langage que l'on utilise, les structures de données peuvent être différentes : il est donc important, pour pouvoir utiliser ces structures dans des pseudo-algorithmes, de se mettre d'accord sur leur comportement.

!!! abstract "Vocabulaire"

    Un **type de donnée abstrait** (TAD) est une spécification mathématique d'un ensemble de données et de l'ensemble des opérations qu'on peut effectuer sur elles. On qualifie d'abstrait ce type de donnée car il ne spécifie pas comment les données sont représentées ni comment les opérations sont implémentées.

    L'**interface** d'une structure de données est la spécification des méthodes pouvant être appliquées sur cette structure de données. L'ensemble des opérations possibles sur ces structures données s'appellent les **primitives**.

    L'**implémentation** d'une structure de données est sa mise en oeuvre pratique dans un langage de programmation.

!!! tip "Important"

    Un TAD ne dépend donc pas de la manière dont la structure de données sera implémentée !

## Un premier exemple : le tableau

Prenons l'exemple du type abstrait **tableau**. Pour concevoir vos algorithmes, vous n'avez pas besoin de savoir spécifiquement comment les tableaux sont implémentés, comment ils utilisent et exploitent la mémoire d'ordinateur. Vous devez simplement connaître les principales opérations, ou primitives, que l'on peut effectuer sur ces tableaux.

Nous supposerons que note tableau est statique et homogène : le nombre de cases de ce tableau est fixé à l'avance et toutes ses entrées seront du même type. En l'occurrence, il est possible :

- de créer un tableau de taille fixée, chaque case étant vide
- de lire dans une case de tableau
- d'écrire dans une case d'un tableau
- d'accéder à la taille d'un tableau

Toute autre opération non spécifiée dans la liste est impossible ! Par exemple, il n'est pas possible de modifier la taille d'un tableau. Les objets de type ```list``` en Python ne sont donc pas des tableaux statiques (d'ailleurs, ce ne sont pas vraiment des listes non plus, mais nous y reviendrons). Pour des données dont la taille peut varier, il est alors préférables d'utiliser d'autres TAD, comme les listes, piles et files que nous allons étudier.


    
## Sources de ce cours

- Cours vidéo d'[Aurélie Lagoutte](https://www.youtube.com/watch?v=B0Ww8KpXcw0)
- Cours de [Frédéric Junier](https://fjunier.forge.aeif.fr/terminale_nsi/T1_Structures_de_donn%C3%A9es/1_Structures_lin%C3%A9aires/C1_Types_Abstraits_Listes/P1_Type_Abstrait/P1_Type_Abstrait/)