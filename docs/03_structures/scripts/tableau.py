def creer_tableau(taille):
    return [None for i in range(taille)]

def lire_case(tableau, index):
    assert isinstance(index, int), "index doit être un entier"
    assert 0 <= index < len(tableau), "index en dehors de la plage licite"
    return tableau[index]

def modifier_case(tableau, index, valeur):
    assert isinstance(index, int), "index doit être un entier"
    assert 0 <= index < len(tableau), "index en dehors de la plage licite"
    tableau[index] = valeur


