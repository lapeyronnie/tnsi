---
author: Jason LAPEYRONNIE
title: Files
---

## Type File

Contrairement aux piles, l'ajout des éléments à une file se fait à une extrémité de celle-ci, tandis que la suppression des éléments se fait à l'autre extrémité de la file. Il s'agit du principe FIFO (pour First In First Out : le premier élément que l'on ajoute à la file sera le premier qui sera supprimé).

![File](images/FIFO_PEPS.png)

Ce type décrit le fonctionnemant d'une file d'attente : les premiers arrivés dans la file sont les premiers à sortir, tandis que d'autres peuvent arriver et se mettent alors en bout de file.


## Interface du type File

!!! abstract "Type de données abstrait : File"

    Les opérations qui peuvent être effectués sur une file sont les suivantes

    |Opération|Description|
    |--|--|
    |creer_file()|Renvoie une file vide|
    |est_file_vide(F)|Renvoie un booléen qui indique si la file ```F``` est vide.|
    |defiler(F)|Renvoie l'élément à la fin de la file **tout en le supprimant de la file**|
    |enfiler(elt, F)|Ajoute l'élément `elt` au début de la file `F` |

    On ajoute parfois à cette interface la possibilité d'accéder à la taille de la file.
    

## Implémentation du type abstrait File

Comme pour la Pile, il est possible d'implémenter le typle `File` à l'aide des `list` en Python.

```python
def creer_file():
    return []

def est_file_vide(F):
    return F == []

def enfiler(elt, F):
    F.insert(0, elt)

def defiler(F):
    assert not est_file_vide(F)
    return F.pop()


def afficher_file(F):
    # Une fonction qui nous permet de visualiser la file
    aux = creer_file()
    affichage = ''
    # On transfère la file F dans la file aux tout en notant les valeurs des éléments
    while not est_file_vide(F) :
        x = defiler(F)
        affichage = "|" + str(x) +"|" + affichage
        enfiler(x, aux)
    # On retransfère les éléments de la file aux dans la file F
    while not est_file_vide(aux):
        enfiler(defiler(aux), F)
    print(affichage)
```

Considérons alors l'exemple suivant

```python
>>> F = creer_file()
>>> est_file_vide(F)
True
>>> empiler(5, F)
>>> empiler(3, F)
>>> empiler(7, F)
>>> defiler(F)
>>> enfiler(9, F)
>>> afficher_file(F)
|9||7||3|
```

Comme pour les piles, nous avons construit la fonction `affiche` uniquement avec les primitives du type File. Nous avons créé une file temporaire où nous stockons les éléments de la file F avant de les remettre dans celle-ci.

