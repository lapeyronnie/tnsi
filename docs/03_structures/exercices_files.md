---
author: Jason LAPEYRONNIE
title: Exercices sur les files
---

## Mayotte 2022, Sujet 1

On cherche ici à mettre en place des algorithmes qui permettent de modifier l’ordre des informations contenues dans une file. On considère pour cela les structures de données abstraites de Pile et File définies par leurs fonctions primitives suivantes :

**Pile :**

- creer_pile_vide() renvoie une pile vide ;
- est_pile_vide(p) renvoie True si la pile p est vide, False sinon ;
- empiler(p, element) ajoute element au sommet de la pile p ;
- depiler(p) renvoie l’élément se situant au sommet de la pile p en le retirant de la pile p ;
- sommet(p) renvoie l’élément se situant au sommet de la pile p sans le retirer de la pile p.

**File :**

- creer_file_vide() renvoie une file vide ;
- est_file_vide(f) renvoie True si la file f est vide, False sinon ;
- enfiler(f, element) ajoute element dans la file f ;
- defiler(f) renvoie l’élément à la tête de la file f en le retirant de la file f.

On considère de plus que l'on dispose d'une fonction permettant de connaître le nombre d'éléments d'une file :

- taille_file(f) renvoie le nombre d'éléments de la file f.

On représentera les files par des éléments en ligne, l'élément de droite étant la tête de la file et l'élément de gauche étant la queue de la file. On représentera les piles en colonnes, le sommet de la pile étant le haut de la colonne.

La file suivante est appelée f : |4|3|8|2|1|


La pile suivante est appelée p :

|p|
|--|
|5|
|8|
|6|
|2|


!!! note "Question 1" 

    Pour chaque question, on repartira de la pile p et de la file f initiales (présentées ci-dessus)
    
    - Représenter la file f après l’exécution du code suivant.

    ```python
    enfiler(f, defiler(f))
    ```

    - Représenter la pile p après l’exécution du code suivant.

    ```python
    empiler(p, depiler(p))
    ```

    - Représenter la pile p et la file f après l’exécution du code suivant.

    ```python
    for i in range(2):
        enfiler(f, depiler(p))
    ```
    
    - Représenter la pile p et la file f après l’exécution du code suivant.

    ```python
    for i in range(2):
        empiler(p, defiler(f))
    ```
    

!!! note "Question 2"  

    On donne ici une fonction mystere qui prend une file en argument, qui modifie cette file, mais qui ne renvoie rien.

    ```python
    def mystere(f):
    p = creer_pile_vide()
    while not est_file_vide(f):
        empiler(p, defiler(f))
    while not est_pile_vide(p):
        enfiler(f, depiler(p))
    return p
    ```

    Préciser l’état de la variable f après chaque boucle de la fonction mystere appliquée à la file . Indiquer le contenu de la pile renvoyée par la fonction.

!!! note "Question 3"

    On considère la fonction knuth(f) suivante dont le paramètre est une file :

    ```python
    def knuth(f):
        p=creer_pile_vide()
        N=taille_file(f)
        for i in range(N):
            if est_pile_vide(p):
                empiler(p, defiler(f))
            else:
                e = defiler(f)
                if e >= sommet(p):
                   empiler(p, e)
               else:
                   while not est_pile_vide(p) and e < sommet(p):
                        enfiler(f, depiler(p))
                    empiler(p, e)
        while not est_pile_vide(p):
            enfiler(f, depiler(p))
    ```

    Recopier et compléter le tableau ci-dessous qui détaille le fonctionnement de cet algorithme étape par étape pour la file 2,1,3. Une étape correspond à une modification de la pile ou de la file. Le nombre de colonnes peut bien sûr être modifié.

    ![Etat des piles et files](images/exo_file04.png)

    Que fait cet algorithme ?


## Centres étrangers 2023, groupe 1

Simon est un jeu de société électronique de forme circulaire comportant quatre grosses touches de couleurs différentes : rouge, vert, bleu et jaune.

Le jeu joue une séquence de couleurs que le joueur doit mémoriser et répéter ensuite. S’il réussit, une couleur parmi les 4 est ajoutée à la fin de la séquence. La nouvelle séquence est jouée depuis le début et le jeu continue. Dès que le joueur se trompe, la séquence est vidée et réinitialisée avec une couleur et une nouvelle partie commence.

Exemple de séquence jouée : rouge -> bleu -> rouge -> jaune -> bleu

Dans cet exercice nous essaierons de reproduire ce jeu.

Les quatre couleurs sont stockées dans un tuple nommé couleurs : `couleurs = ("bleu", "rouge", "jaune", "vert")`

Pour stocker la séquence à afficher nous utiliserons une structure de file que l’on nommera `sequence` tout au long de l’exercice.

La file est une structure linéaire de type FIFO (First In First Out). Nous utiliserons durant cet exercice les fonctions suivantes :


- creer_file_vide() : renvoie une file vide
- est_vide(f) : renvoie True si f est vide, False sinon
- enfiler(f, element) : ajoute element en queue de f
- defiler(f) : retire l'élément en tête de f et le renvoie
- taille(f) : renvoie le nombre d'éléments de f

En fin de chaque séquence, le Simon tire au hasard une couleur parmi les 4 proposées.

On utilisera la fonction `randint(a,b)` de la bibliothèque `random`` qui permet d’obtenir un nombre entier compris entre a inclus et b inclus pour le tirage aléatoire.

Exemple : randint(1,5) peut renvoyer 1, 2, 3, 4 ou 5.

!!! note "Question 1"

    Recopier et compléter, sur votre copie, les ... des lignes 3 et 4 de la fonction ajout(f) qui permet de tirer au hasard une couleur et de l’ajouter à une séquence.

    La fonction ajout prend en paramètre la séquence f ; elle renvoie la séquence f modifiée (qui intègre la couleur ajoutée au format chaîne de caractères).

    ```python
    def ajout(f):
        couleurs = ("bleu", "rouge", "jaune", "vert")
        indice = randint(...,...)
        enfiler(...,...)
        return f
    ```

En cas d’erreur du joueur durant sa réponse, la partie reprend au début ; il faut donc
vider la file sequence pour recommencer à zéro en appelant vider(sequence) qui
permet de rendre la file sequence vide sans la renvoyer.

!!! note "Question 2"

    Ecrire la fonction vider qui prend en paramètre une séquence f et la vide sans la renvoyer.

Le Simon doit afficher successivement les différentes couleurs de la séquence.

Ce rôle est confié à la fonction `affich_seq(sequence)`, qui prend en paramètre la file de couleurs `sequence`, définie par l’algorithme suivant :

- on ajoute une nouvelle couleur à sequence ;
- on affiche les couleurs de la séquence, une par une, avec une pause de 0,5 s entre chaque affichage.

Une fonction `affichage(couleur)` (dont la rédaction n’est pas demandée dans cet exercice) permettra l’affichage de la couleur souhaitée avec couleur de type
chaîne de caractères correspondant à une des 4 couleurs.

La temporisation de 0,5 s sera effectuée avec la commande time.sleep(0.5). Après l’exécution de la fonction `affich_seq`, la file sequence ne devra pas être
vidée de ses éléments.


!!! note "Question 3"

    Recopier et compléter, sur la copie, les ... des lignes 4 à 10 de la fonction `affich_seq(sequence)` ci-dessous :

    ```python
    def affich_seq(sequence):
        stock = creer_file_vide()
        ajout(sequence)
        while not est_vide(sequence) :
            c = ...
            ...
            time.sleep(0.5)
            ...
        while ... :
            ...
    ```

!!! note "Question 4" 

    Cette question est indépendante des précédentes : bien qu’elle fasse appel aux fonctions construites précédemment, elle peut être résolue même si le candidat n’a pas réussi toutes les questions précédentes.

    Nous allons ici créer une fonction `tour_de_jeu(sequence)` qui gère le déroulement d’un tour quelconque de jeu côté joueur. La fonction `tour_de_jeu` prend en paramètre la file de couleurs `sequence``, qui contient un certain nombre de couleurs.

    - Le jeu électronique Simon commence par ajouter une couleur à la séquence et affiche l’intégralité de la séquence. 
    - Le joueur doit reproduire la séquence dans le même ordre. Il choisit une couleur via la fonction saisie_joueur().
    - On vérifie si cette couleur est conforme à celle de la séquence.
    - S’il s’agit de la bonne couleur, on poursuit sinon on vide sequence.
    - Si le joueur arrive au bout de la séquence, il valide le tour de jeu et le jeu se poursuit avec un nouveau tour de jeu, sinon le joueur a perdu et le jeu s’arrête.

    La fonction tour_de_jeu s’arrête donc si le joueur a trouvé toutes les bonnes couleurs de sequence dans l’ordre, ou bien dès que le joueur se trompe.

    Après l’exécution de la fonction tour_de_jeu, la file sequence ne devra pas être vidée de ses éléments en cas de victoire.

    **a.** Afin d’obtenir la fonction tour_de_jeu(sequence) correspondant au comportement décrit ci-dessus, recopier le script ci-dessous et :
    
    - Compléter le ...
    - Choisir parmi les propositions de syntaxes suivantes lesquelles correspondent aux ZONES A, B, C, D, E et F figurant dans le script et les y remplacer (il ne faut donc en choisir que six parmi les onze) :

        - vider(sequence)
        - defiler(sequence)
        - enfiler(sequence,c_joueur)
        - enfiler(stock,c_seq)
        - enfiler(sequence, defiler(stock))
        - enfiler(stock, defiler(sequence))
        - affich_seq(sequence)
        - while not est_vide(sequence):
        - while not est_vide(stock):
        - if not est_vide(sequence):
        - if not est_vide(stock):

    ```python
    def tour_de_jeu(sequence):
        ZONE A
        stock = creer_file_vide()
        while not est_vide(sequence) :
            c_joueur = saisie_joueur()
            c_seq = ZONE B
            if c_joueur ... c_seq:
                ZONE C
            else:
                ZONE D
        ZONE E
            ZONE F
    ```

    **b.** Proposer une modification pour que la fonction se répète si le joueur trouve toutes les couleurs de la séquence (dans ce cas, une nouvelle couleur est ajoutée) ou s’il se trompe (dans ce cas, la séquence est vidée et se voit ajouter une nouvelle couleur). On pourra ajouter des instructions qui ne sont pas proposées dans la question a.

## Métropole, Antilles, Guyane, Septembre2022

Voir exercice 5 : [NSI Septembre 2022](https://pixees.fr/informatiquelycee/term/suj_bac/2022/sujet_14.pdf)
