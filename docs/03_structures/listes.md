---
author: Jason LAPEYRONNIE
title: Listes
---

!!! danger "Attention"

    Le Type Abstrait Liste et le type de données ```list``` en Python sont deux choses **différentes** !

## Type Liste

!!! abstract "Type de données abstrait : Liste"

    Le Type abstrait Liste permet de créer une séquence d'éléments ordonnés par leur position dans la liste. Les opérations qui peuvent être effectués sont les suivantes

    |Opération|Description|
    |--|--|
    |creer_liste()|Renvoie une liste vide|
    |est_vide(L)|Renvoie un booléen qui indique si la liste ```L``` est vide.|
    |tete(L)|Renvoie l'élément en tête de liste|
    |queue(L)|Renvoie une liste qui correspond à la liste L privée de son premier élément|
    |inserer(L,elt)|Insère ```elt``` en tête de la liste ```L```|

Contrairement au tableau que nous avons vu précédemment, le type abstrait Liste peut varier en taille. Cependant, il n'est pas possible d'accéder directement à la taille de la liste : il faut pour cela parcourir l'ensemble de ces élèments.

## Implémentation du type Liste

Nous allons désormais implémenter le type Liste en Python. Une possibilité est la suivante :

```python
def creer_liste():
    return None

def est_vide(L):
    return L is None

def tete(L):
    assert not est_vide(L), "La liste ne doit pas être vide"
    return L[0]

def queue(L):
    assert not est_vide(L), "La liste ne doit pas être vide"
    return L[1]

def inserer(L,elt):
    return (elt,L)
```

La construction d'une liste est donc récursive : une liste de taille n est la donnée d'un élément de tête et d'une liste de taille n-1. Grâce aux fonctions que nous venons d'implémenter, nous pouvons alors utiliser cette implémentation pour créer et modifier des listes

```python
>>> L = creer_liste()
>>> est_vide(L)
True
>>> L = inserer(L,5)
>>> L = inserer(L,7)
>>> L = inserer(L,9)
>>> L
(9, (7, (5, None)))
```

Rien ne nous empêche alors d'écrire de nouvelles fonctions qui utiliseront les fonctions primitives précédentes. Par exemple, si nous souhaitons créer une fonction qui prend en entrée une Liste et qui renvoie le nombre d'éléments de cette liste, nous pouvons le faire comme suit (la structure récursive de la `Liste` nous oriente naturellement vers un algorithme récursif).

```python
def longueur(L):
    if est_vide(L) :
        return 0
    else :
        return 1 + longueur(queue(L))
```

Par ailleurs, contrairement au tableau, l'accès à un élément de la liste ne peut se faire immédiatement. Si je souhaite accéder au troisième élément de la liste, il me faut par exemple entrer 

```python
tete(queue(queue(L)))
```