---
author: Jason LAPEYRONNIE
title: Piles
---

## Type Pile

Les piles sont des structures de données très similaires aux listes. Dans une pile, il est uniquement possible de manipuler le dernier élément qui y a été introduit : c'est le principe du Last In First Out, ou tout simplement LIFO.

Le principe est le même que pour des cubes que l'on empile. Il est alors uniquement possible d'interagir avec le cube posé sur le sommet. On peut alors ajouter des cubes sur le sommet de la pile (on dit que l'on empile) ou, au contraire, retirer le cube au sommet de cette pile (et on dit dans ce cas que l'on dépile).

La structure de Pile se rencontre dans plusieurs situations

- nous en avons déjà eu un aperçu lors du premier chapitre sur la récursivité, en évoquant la pile des apells successifs,
- lors d'une navigation sur Internet, les différentes pages visitées s'empilent dans l'historique. Ceci permettant notamment de revenir à la page précédente d'un simlple clic
- lors de l'édition d'un document, les dernières actions effectuées sont empilées. Ceci permet notamment d'annuler les dernières actions à l'aide de la commande Ctrl + Z.


## Interface du type Pile

!!! abstract "Type de données abstrait : Pile"

    Les opérations qui peuvent être effectués sur une pile sont les suivantes

    |Opération|Description|
    |--|--|
    |creer_pile()|Renvoie une pile vide|
    |est_pile_vide(P)|Renvoie un booléen qui indique si la pile ```P``` est vide.|
    |depiler(P)|Renvoie l'élément au sommet de la pile **tout en le supprimant de la pile**|
    |sommet(P)|Renvoie l'élément au sommet de la pile **sans le supprimaer de la pile**|
    |empiler(elt, P)|Ajoute l'élément `elt` au sommet de la pile `P` |

    On ajoute parfois à cette interface la possibilité d'accéder à la taille de la pile
    

## Implémentation du type abstrait Pile

Une possibilité d'implémentation du type abstrait Pile et utilisant le type Python `list` est la suivante :

```python
def creer_pile():
    return []

def est_pile_vide(P):
    return P == []

def empiler(elt, P):
    P.append(elt)

def depiler(P):
    assert not est_pile_vide(P)
    return P.pop()

def sommet(P):
    return P[-1]

def afficher_pile(P):
    # Une fonction qui nous permet de visualiser la pile
    aux = creer_pile()
    affichage = ''
    # On transfère la pile P dans la pile aux tout en notant les valeurs des éléments
    while not est_pile_vide(P) :
        affichage += "|\t" + str(sommet(P)) + "\t|\n"
        empiler(depiler(P), aux)
    # On retransfère les éléments de la pile aux dans la pile P
    while not est_pile_vide(aux):
        empiler(depiler(aux), P)
    print(affichage)
```

Considérons alors l'exemple suivant

```python
>>> P = creer_pile()
>>> est_pile_vide(P)
True
>>> empiler(5, P)
>>> empiler(3, P)
>>> empiler(7, P)
>>> sommet(P)
7
>>> depiler(P)
>>> sommet(P)
3
>>> empiler(9, P)
>>> afficher_pile(P)
|   9   |
|   3   |
|   5   |
```

Remarquez la construction de la fonction `affiche` en utilisant les primitives du type Pile : celle-ci crée une pile temporaire dans laquelle on verse la pile P : la pile P se trouve alors en sens inverse ! Avant que la fonction ne se termine, la pile est de nouveau transférée dans la pile de départ. Ce fonctionnement est extrêmemnt courant lorsque l'on souhaite accéder à l'ensemble des éléments de la pile. Voici par exemple comment l'on peut implémenter une fonction qui renvoie la taille de la pile

```python
def taille(P):
    aux = creer_pile()
    cpt = 0
    # On transfère la pile P dans la pile aux tout en ajoutant 1 au compteur pour chaque élément
    while not est_pile_vide(P) :
        cpt += 1
        empiler(depiler(P), aux)
    # On retransfère les éléments de la pile aux dans la pile P
    while not est_pile_vide(aux):
        empiler(depiler(aux), P)
    return cpt
```
