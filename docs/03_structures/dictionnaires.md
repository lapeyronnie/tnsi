---
author: Jason LAPEYRONNIE
title: Dictionnaires
---

## Dictionnaire

Un dictionnaire (ou tableau associatif) est un type de données en Python qui permet d'associer des clés et des valeurs.

Les clés d'un dictionnaire peuvent être de différents types, pourvu que celui-ci soit non mutable : entiers, flottants, chaînes de caractère ou tuples d'entiers, flottants et chaînes. En revanche, un tableau ne peut pas être utilisé comme clé d'un dictionnaire. Le dictionnaire n'étant pas indexé sur des entiers, il ne s'agit donc pas d'une séquence (contrairement aux tableaux et aux tuples).

Les valeurs du dictionnaire peuvent être de n'importe quel type.

On peut alors accéder à n'importe quelle valeur à partir de la clé.

!!! abstract "Type de données abstrait : Dictionnaire"

    Les opérations qui peuvent être effectués sur un dictionnaire sont les suivantes

    |Opération|Description|
    |--|--|
    |creer_dict()|Renvoie un dictionnaire vide.|
    |ajouter(D, c, v)|Associe la clé à la valeur v dans le dictionnaire D.|
    |modifier(D, c, v)|Remplace la valeur déjà existante associée à la clé c par la valeur v dans le dictionnaire D.|
    |supprimer(D, c)|Supprime la clé c et sa valeur associée dans le dictionnaire D. |
    |rechercher(D, c)|Renvoie la valeur associée à la clé c dans le dictionnaire D, si celle-ci existe.|

L'important dans un dictionnaire est que la recherche dans celui-ci se fait à **complexité constante*** (contrairement à la recherche dans un tableau non trié qui présente une complexité lilnéaire).

## Dictionnaire en Python

En Python, un dictionnaire se construit à l'aide d'accolades. Les couples clé/valeur sont séparés par deux points.

```python
dupont = {"nom" : "Dupont", "prénom" : "Machin", "discipline" : "nsi", "notes" : [12, 15, 17, 13], "annee" : 2004}
```

Dans cet exemple, on crée un dictionnaire nommé **dupont**.

- les clés de ce dictionnaires sont "nom", "prénom", "discipline", "notes" et "annee"
- la valeur associé à la clé "discipline" est le chaîne de caractère "nsi"
- la valeur associé à la clé "notes" est le tableau [12, 15, 17, 13]

Si je souhaite accéder à la valeur de la clé "prénom", il me suffit de faire ainsi :

```python
dupont['discipline']```

Il est également possible de procéder à partir d'un dictionnaire vide et d'ajouter les couples clé/valeur du dictionnaire par extension

```python
#On commence par créer un dictionnaire vide
dic = {}

#On crée la clé 'taille' à laquelle on associe la valeur 165
dic['taille'] = 165

#On crée la clé 'age' à laquelle on associe la valeur 16
dic['age'] = 16

#Si une clé existe déjà, une nouvelle assignation met la valeur de la clé à jour
dic['age'] = 17

#Il est aussi possible d'utiliser des opérations pour la mise à jour
dic['age'] += 1
```

Comme les listes, un dictionnaire peut également être créé par compréhension

```python
>>> dic = {x : x**2 for x in range(7)}
>>> print(dic)
{0: 0, 1: 1, 2: 4, 3: 9, 4: 16, 5: 25, 6: 36}
```

Pour supprimer un élément d'un dictionnaire, on utilisera del

```python
>>> dupont = {"nom" : "Dupont", "prénom" : "Machin", "discipline" : "nsi", "notes" : [12, 15, 17, 13], "annee" : 2004}
>>> print(dupont)
{'nom': 'Dupont', 'prénom': 'Machin', 'discipline': 'nsi', 'notes': [12, 15, 17, 13], 'annee': 2004}
>>> del dupont["notes"]
>>> print(dupont)
{'nom': 'Dupont', 'prénom': 'Machin', 'discipline': 'nsi', 'annee': 2004}
```

## Parcours d'un dictionnaire

### Parcours des clés

Pour parcourir les clés d'un dictionnaire **dic**, on utilisera la méthode **.keys()**. **dic.keys()** renvoie une liste contenant toutes les clés du dictionnaire **dic**.

```python
fruits = {"pomme" : 3, "banane" : 7, "orange" : 2, "poire" : 4}
for cle in fruits.keys():
    print(cle)
```

Ce programme affichera

```python
pomme
banane
orange
poire
```


### Parcours des valeurs

Pour parcourir les valeur d'un dictionnaire, on utilisera la méthode values(). dic.values() renvoie une liste contenant toutes les valeurs du dictionnaire dic.

```python
fruits = {"pomme" : 3, "banane" : 7, "orange" : 2, "poire" : 4}
for valeur in fruits.values():
    print(valeur)
```

Ce programme affichera

```python
3
7
2
4
```

### Parcours des couples clé-valeur

Pour parcourir les couples clé-valeur d'un dictionnaire, on utilisera la méthode **items()**. Les items sont alors des tuples et il faudra par exemple inclure deux objets dans la boucle.

```python
fruits = {"pomme" : 3, "banane" : 7, "orange" : 2, "poire" : 4}
for cle, valeur in fruits.items():
    print(cle + " : "+str(valeur))
```

Ce programme affichera

```python
pomme : 3
banane : 7
orange : 2
poire : 4
```

## Exemple d'utilisation : compte d'éléments

On souhaite construire une fonction comptefruit qui prend en entrée une liste, de taille inconnue, et dont les éléments sont des noms de fruits. La fonction comptefruit renvoie un dictionnaire dont les clés sont les fruits et dont les valeurs sont le nombre de fois où le fruit apparaît dans la liste.

```python
def comptefruit(tab) :
    #On initialise un dictionnaire vide
    compte = {}
    for fruit in tab :
        #si le fruit n'est pas présent dans le dictionnaire, on l'ajoute
        if fruit not in compte.keys() :
            compte[fruit] = 1
        #sinon, on ajoute 1 dans le compte correspondant
        else :
            compte[fruit] += 1
    return compte

#Testons notre fonction : les lignes suivantes permettent de générer un tableau dont les éléments sont pris au hasard parmi 
# 'pomme', 'poire', 'banane', 'orange'. Le tableau généré comprend entre 30 et 50 éléments.

import random

nombre = random.randint(30, 50)
listefruit = ['pomme', 'orange', 'banane', 'poire']
testlist = random.choices(listefruit, k = nombre)
print(testlist)

#On appelle alors la fonction comptefruit en lui donnant en argument la liste générée.
comptefruit(testlist)
```

Une exécution de ce programme a donné la résultat suivant

```python

['banane', 'poire', 'poire', 'poire', 'banane', 'pomme', 'pomme', 'pomme', 'pomme', 'banane', 'poire', 'poire', 'orange', 'poire', 'banane', 'orange', 'banane', 'banane', 'orange', 'banane', 'pomme', 'orange', 'poire', 'banane', 'orange', 'poire', 'pomme', 'banane', 'banane', 'poire', 'pomme', 'orange', 'poire', 'pomme']

{'banane': 10, 'poire': 10, 'pomme': 8, 'orange': 6}
```