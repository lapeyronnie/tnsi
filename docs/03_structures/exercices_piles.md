---
author: Jason LAPEYRONNIE
title: Exercices sur les piles
---

## Centres étrangers groupe 1, juin 2021

Dans cet exercice, on considère une pile d'entiers positifs. On suppose que les quatre fonctions suivantes ont été programmées préalablement en langage Python :

- `empiler(P, e)` : ajoute l'élément e sur la pile P ;
- `depiler(P)` : enlève le sommet de la pile P et retourne la valeur de ce sommet ;
- `est_vide(P)` : retourne True si la pile est vide et False sinon ;
- `creer_pile()` : retourne une pile vide

!!! note "Question 1"

    Recopier le schéma ci-dessous et le compléter en exécutant les appels de fonctions donnés. On écrira ce que renvoie la fonction utilisée dans chaque cas, et on indiquera `None` si la fonction ne retourne aucune valeur.

![Pile](images/exo_pile01.png)

On propose la fonction ci-dessous, qui prend en argument une pile P et renvoie un couple de piles :

```python
def transforme(P) :
    Q = creer_pile()
    while not est_vide(P) :
        v = depile(P)
        empile(Q,v)
    return (P,Q)
```

!!! note "Question 2"

    Recopier et compléter le document ci-dessous.

![Transformation de Pile](images/exo_pile02.png)

!!! note "Question 3"

    Ecrire une fonction en langage Python `maximum(P)` recevant une pile `P` comme argument et qui renvoie la valeur maximale de cette pile. On ne s’interdit pas qu’après exécution de la fonction, la pile soit vide.

## Métropole Antilles Guyane, mai 2022

On ne tiendra pas compte dans cette partie des balises ne comportant pas de fermeture comme `<br>` ou `<img ...>`.

Afin de vérifier qu’une expression HTML simplifiée est correctement balisée, on peut utiliser une pile (initialement vide) selon l’algorithme suivant :

On parcourt successivement chaque balise de l’expression :

- lorsque l’on rencontre une balise ouvrante, on l’empile ;
- lorsque l’on rencontre une balise fermante :
    - si la pile est vide, alors l’analyse s’arrête : le balisage est incorrect ,
    - sinon, on dépile et on vérifie que les deux balises (la balise fermante rencontrée et la balise ouvrante dépilée) correspondent (c’est-à-dire ont le même nom) si ce n’est pas le cas, l’analyse s’arrête (balisage incorrect).

**Exemple** : État de la pile lors du déroulement de cet algorithme pour l’expression simplifiée "`<p><em></p></em>`" qui n’est pas correctement balisée.

![Balisage HTML](images/exo_pile03.png)

!!! note "Questions"

    1. Représenter la pile à chaque étape du déroulement de cet algorithme pour l’expression "`<p><em></em></p>`" (balisage correct).
    2. Indiquer quelle condition simple (sur le contenu de la pile) permet alors de dire que le balisage est correct lorsque toute l’expression HTML simplifiée a été entièrement parcourue, sans que l’analyse ne s’arrête.
    3. Une expression HTML correctement balisée contient 12 balises. Indiquer le nombre d’éléments que pourrait contenir au maximum la pile lors de son analyse.

## Mayotte, 19 mai 2022

La notation polonaise inverse (NPI) permet d'écrire des expressions de calculs numériques sans utiliser de parenthèse. Cette manière de présenter les calculs a été utilisée dans des calculatrices de bureau dès la fin des années 1960. La NPI est une forme d’écriture d’expressions algébriques qui se distingue par la position relative que prennent les nombres et leurs opérations.

Par exemple :

|Notation classique| Notation NPI|
|--|--|
|3+9| 3 9 +|
|8×(3+5)| 8 3 5 + ×|
|(17+5)×4 |17 5 + 4 ×|

L’expression est lue et évaluée de la gauche vers la droite en mettant à jour une pile.

- Les nombres sont empilés dans l’ordre de la lecture.
- Dès la lecture d’un opérateur (+, -, ×, /), les deux nombres au sommet de la pile sont dépilés et remplacés par le résultat de l’opération effectuée avec ces deux nombres. Ce résultat est ensuite empilé au sommet de la pile.

A la fin de la lecture, la valeur au sommet est renvoyée.

Exemple : l’expression 7 3 25 + ×`` qui correspond au calcul 7×(3+25)`` s’évalue à 196 comme le montrent les états successifs de la pile créée, nommée p :

- On empile la valeur 7.
- On empile la valeur 3.
- On empile la valeur 25.
- On remplace les deux nombres du sommet de la pile (25 et 3) par leur somme 28.
- On remplace les deux nombres du sommet de la pile (28 et 7) par leur produit 196.

**Schéma descriptif des différentes étapes d’exécution.**

![Notation polonaise inverséé](images/exo_pile04.png)

!!! note "Question 1"

    En vous inspirant de l’exemple ci-dessus, dessiner le schéma descriptif de ce que donne l’évaluation par la NPI de l’expression `12 4 5 × +` .


On dispose de la pile suivante nommée p1 : 
    
![Pile p1](images/exo_pile05.png)
    
On rappelle ci-dessous les primitives de la structure de pile (LIFO : Last In First out) :

|Fonction| Description|
|--|--|
|pile_vide()| Créé et renvoie une nouvelle pile vide|
|empiler(p, e)| Place l’élément e au sommet de la pile p.|
|depiler(p)| Supprime et renvoie l’élément se trouvant au sommet de p.|
|est_vide(p) |Renvoie un booléen indiquant si p est vide ou non.|

On dispose aussi de la fonction suivante, qui prend en paramètre une pile p :

```python
def top(p) :
    x = depiler(p)
    empiler(p, x)
    return x
```

!!! note "Question 2"

    On exécute la ligne suivante temp = top(p1) :

    1. Quelle valeur contient la variable temp après cette exécution ?
    2. Représenter la pile p1 après cette exécution.

!!! note "Question 3"

    En utilisant uniquement les 4 primitives d’une pile, écrire en langage Python la fonction `addition(p)` qui prend en paramètre une pile `p` d’au moins deux éléments et qui remplace les deux nombres du sommet d’une pile `p` par leur somme.

    **Remarque** : cette fonction ne renvoie rien, mais la pile p est modifiée.

!!! note "Question 4"

    On considère que l’on dispose également d’une fonction `multiplication(p)` qui remplace les deux nombres du sommet d’une pile `p` par leur produit (on ne demande pas d’écrire cette fonction). Recopier et compléter, en n’utilisant que les primitives d’une pile et les deux fonctions `addition` et `multiplication`, la suite d’instructions (ci-dessous) qui réalise le calcul `(3+5)×7` dont l’écriture en NPI est : `3 5 + 7 ×`

    ```python
    p=pile_vide()
    empiler(p,3)
    ```

## BNS sujet 2, exercice 2

Cet exercice utilise des piles qui seront représentées en Python par des listes (type `list`).

On rappelle que l’expression `liste_1 = list(liste)` fait une copie de `liste `indépendante de `liste`, que
l’expression `x = liste.pop()` enlève le sommet de la pile `liste` et le place dans la variable `x` et,
enfin, que l’expression `liste.append(v)` place la valeur `v` au sommet de la pile `liste`.

Compléter le code Python de la fonction `positif` ci-dessous qui prend une pile `liste` de
nombres entiers en paramètre et qui renvoie la pile des entiers positifs dans le même
ordre, sans modifier la variable `liste`.

```python
>>> positif([-1, 0, 5, -3, 4, -6, 10, 9, -8])
[0, 5, 4, 10, 9]
>>> positif([-2])
[]
```

???+ question "Programmez !"

    {{IDE('scripts/positif')}}