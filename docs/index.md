# Terminale NSI - Jason Lapeyronnie
Vous trouverez sur ce site mes cours et exercices à destination des élèves de Terminale Générale ayant choisi la spécialité NSI.

Les sites suivants m'ont servi de référence pour construire les différentes pages de ce site.

- Le site [Pixees](https://pixees.fr/informatiquelycee/term/) de David Roche. Vous y trouverez notamment les énoncés et corrigés des épeuves écrites et épreuves pratiques
- Le site [Mon Lycée Numérique](www.monlyceenumerique.fr/index_nsi.html)
- Le site de [Frédéric Junier](https://fjunier.forge.aeif.fr/terminale_nsi/)
- Le site du [Lycée Vaugelas de Chambéry](http://193.49.249.136:20180/~web/terminale/index.php)


