---
author: Jason Lapeyronnie
title: Le Langage SQL
---

L'objectif de ce cours est d'apprendre à manipuler le langage SQL (Structured Query Language). Il s'agit d'un langage propre aux bases de données relationnelles dont certaines subtilités varient selon le SGBD utilisé. 

Dans toute cette partie, nous considérons les deux relations *élèves* et *classes* vues dans la partie précédente.

Relation **élèves**

| numéro| prénom       | nom      | j_nais | m_nais | a_nais | classe |
|-----------------|---------------|----------|-------------------|-------------------|--------------------|--------|
| 12345678        | Sofia         | Ramirez  | 5                 | 1                 | 2006               | TGA    |
| 23456789        | Malik         | Khan     | 12                | 3                 | 2006               | TGB    |
| 24362736        | Luna          | Petrov   | 20                | 4                 | 2005               | TGC    |
| 25746380        | Andrei        | Popescu  | 8                 | 7                 | 2006               | TGA    | 
| 29038746       | Isabella      | Costa    | 15                | 9                 | 2006               | TGB    |
| 30948637        | Kai           | Nakamura | 3                 | 11                | 2006               | TGC    |

Relation **classes**

| nom_classe | pp1 | pp2 |
|-----------------|---------------|----------|
| TGA    | Machin              | Chouette           |
| TGB    | Bidule             | Quidam              |
| TGC    | Chose              | Exemple            |

## Construction de la base de données

Avant même d'exploiter la base de données, encore faut-il que celle-ci soit créée. Cette opération se fait à l'aide du mot-clé **CREATE TABLE**. On précise ensuite les attributs de la relation, ainsi que leur domaine. Cette précision peut être agrémentée de contraintes : on peut par exemple préciser d'emblée quel sera l'attribut qui fera office de clé primaire ou l'on pourra demander explicitement à ce qu'une information de la table ne puisse pas être NULL.

Comme nous l'avons vu précédemment, il est préférable de créer en premier la table *classes*, puisque la table *élèves* fait référence à cette table.

```sql
CREATE TABLE classes (
    nom_classe TEXT PRIMARY KEY NOT NULL,       -- on définit l'attribut nom_classe comme clef primaire
    pp1 TEXT NOT NULL,                        -- le NOT NULL signifie qu'on n'accepte pas d'absence de valeur pour tel attribut
    pp2 TEXT
    );
```

La création de la table *élèves* se fait de manière tout à fait similaire, en ajoutant la définition d'une clé étrangère.

```sql
CREATE TABLE élèves (
    numéro INTEGER PRIMARY KEY NOT NULL,    
    prénom TEXT NOT NULL,
    nom TEXT NOT NULL,
    j_nais INT,
    m_nais INT,
    a_nais INT,
    classe TEXT REFERENCES classes (nom_classe),  -- on définit l'attribut classe comme clef étrangère
    );
```

La structure étant fixée, il faut désormais remplir ces tables à l'aide de la commande **INSERT INTO table VALUES (enregistrement)** LEs chaînes de caractères doivent être entourées de guillemets. Il est par ailleurs possible de réaliser plusieurs enregistrements simultanément, en les séparant par des virgules.

```sql
INSERT INTO classes VALUES
('TGA', 'Machin', 'Chouette'),
('TGB', 'Bidule', 'Quidam'),
('TGC', 'Chose', 'Exemple')
```

Dans la mesure où l'attribut pp2 n'est pas renseigné en tant que NOT NULL, il est possible de laisser cet attribut vide lors d'un enregistrement. Il suffit de renseigner NULL comme suit

```sql
INSERT INTO classes VALUES
('TGD', 'Autre', NULL),
```

On procède de la même manière pour remplir la table *élèves*

```sql
INSERT INTO élèves VALUES
(12345678,'Sofia','Ramirez',5,1,2006,'TGA'),
(23456789,'Malik','Khan',12,3,2006,'TGB'),
(24362736,'Luna','Petrov',20,4,2005,'TGC'),
(25746380,'Andrei','Popescu',8,7,2006,'TGA'), 
(29038746,'Isabella','Costa',15,9,2006,'TGB'),
(30948637,'Kai','Nakamura',3, 11, 2006,'TGC')
```

Les contraintes sont alors mises en place suivant les instructions données lors de la création des tables. Par exemple, l'attribut *numéro* a été défini comme clé primaire de la table *élèves*. Ainsi, ajouter un enregistrement ayant un numéro déjà attribué provoquera une erreur.

```sql
INSERT INTO élèves VALUES
(30948637,'Covert','Harry',7, 8, 2006,'TGC')
```

L'exécution de cette commande provoquera l'erreur ```UNIQUE constraint failed```. De la même manière, ajouter à la table *classes* un enregistrement dont le *nom_classe* est déjà pris provoquera le même type d'erreur.

Par ailleurs, la commande suivante provoquera également une erreur.

```sql
INSERT INTO élèves VALUES
(30948630,'Covert','Harry',7, 8, 2006,'TGE')
```

Cette fois, il s'agira d'une ```FOREIGN KEY constraint failed``` : en effet, la classe TGE ne figure pas dans la table *classes*, il est donc impossible d'y faire référence.

## Interrogation de la base de données

### Interrogations simples

La base de données étant constituée, il est désormais possible de l'interroger en effectuant des requêtes (doù le nom du langage, SQL, query signifiant requête en anglais). Pour visualiser une table, il suffit d'utiliser la requête **SELECT * FROM table**.

```sql
SELECT * FROM élèves
```

Cette requête nous permet d'obtenir l'ensemble des enregistrements de la table *élèves*.

| numéro| prénom       | nom      | j_nais | m_nais | a_nais | classe |
|-----------------|---------------|----------|-------------------|-------------------|--------------------|--------|
| 12345678        | Sofia         | Ramirez  | 5                 | 1                 | 2006               | TGA    |
| 23456789        | Malik         | Khan     | 12                | 3                 | 2006               | TGB    |
| 24362736        | Luna          | Petrov   | 20                | 4                 | 2005               | TGC    |
| 25746380        | Andrei        | Popescu  | 8                 | 7                 | 2006               | TGA    | 
| 29038746        | Isabella      | Costa    | 15                | 9                 | 2006               | TGB    |
| 30948637        | Kai           | Nakamura | 3                 | 11                | 2006               | TGC    |


Il se peut toutefois que je n'ai pas envie d'accéder à l'ensemble des informations présentes dans cette table. Par exemple, si je souhaite uniquement les prénoms, noms et classes des élèves, je peux effectuer la requête suivante

```sql
SELECT prénom, nom, classe FROM élèves
```

qui permettra d'obtenir le résultat suivant

|  prénom       | nom      | classe |
|---------------|----------|--------|
| Sofia         | Ramirez  | TGA    |
| Malik         | Khan     | TGB    |
| Luna          | Petrov   | TGC    |
| Andrei        | Popescu  | TGA    | 
| Isabella      | Costa    | TGB    |
| Kai           | Nakamura | TGC    |

### Sélection (WHERE)

Lorsque l'on souhaite délectionner seulement une partie des données, il est possible d'utiliser la clause **WHERE** afin d'imposer une ou des condition(s) sur les enregistrements à sélectionner. On peut par exemple seulement vouloir les prénoms et noms des élèves inscrits en classe de TGA. Pour cela, on peut utiliser la commande suivante

```sql
SELECT prénom, nom, classe FROM élèves WHERE classe = 'TGA'
```

|  prénom       | nom      | classe |
|---------------|----------|--------|
| Sofia         | Ramirez  | TGA    |
| Andrei        | Popescu  | TGA    | 

Il est possible de combiner plusieurs conditions avec les mots-clés OR et AND. La commande suivante permet de sélectionner les élèves nés entre september et décembre 2006.

```sql
SELECT prénom, nom, classe FROM élèves WHERE a_nais = 2006 AND m_nais >= 9
```

|  prénom       | nom      | classe |
|---------------|----------|--------|
| Isabella      | Costa    | TGB    |
| Kai           | Nakamura | TGC    |

### Tri (ORDER BY)

Les résultats d'une requête peuvent être classés par oredre croissant ou décroissant grâce à la clause **ORDER BY**. Il suffit de faire suivre ce mot-clé du nom de l'attribut sur lequel est effectué le classement. Par défaut, le classement est effectué par ordre croissant.

La commande suivante permet de trier les lignes selon l'ordre croissant des noms

```sql
SELECT * FROM élèves ORDER BY nom
```

| numéro| prénom       | nom      | j_nais | m_nais | a_nais | classe |
|-----------------|---------------|----------|-------------------|-------------------|--------------------|--------|
| 29038746        | Isabella      | Costa    | 15                | 9                 | 2006               | TGB    |
| 23456789        | Malik         | Khan     | 12                | 3                 | 2006               | TGB    |
| 30948637        | Kai           | Nakamura | 3                 | 11                | 2006               | TGC    |
| 24362736        | Luna          | Petrov   | 20                | 4                 | 2005               | TGC    |
| 25746380        | Andrei        | Popescu  | 8                 | 7                 | 2006               | TGA    | 
| 12345678        | Sofia         | Ramirez  | 5                 | 1                 | 2006               | TGA    |

Pour trier dans l'ordre décroissant, il faut ajouter le mot-clé **DESC**

```sql
SELECT * FROM élèves ORDER BY nom DESC
```

| numéro| prénom       | nom      | j_nais | m_nais | a_nais | classe |
|-----------------|---------------|----------|-------------------|-------------------|--------------------|--------|
| 12345678        | Sofia         | Ramirez  | 5                 | 1                 | 2006               | TGA    |
| 25746380        | Andrei        | Popescu  | 8                 | 7                 | 2006               | TGA    | 
| 24362736        | Luna          | Petrov   | 20                | 4                 | 2005               | TGC    |
| 30948637        | Kai           | Nakamura | 3                 | 11                | 2006               | TGC    |
| 23456789        | Malik         | Khan     | 12                | 3                 | 2006               | TGB    |
| 29038746        | Isabella      | Costa    | 15                | 9                 | 2006               | TGB    |

Il est également possible de préciser plusieurs attributs pour réaliser le tri. Les lignes seront alors triés selon le premier attribut donné, puis selon le deuxième en cas d'égalité, et ainsi de suite. On peut par exemple trier les données selon l'année, puis le mois et enfin le jour de naissance comme suit.

```sql
SELECT * FROM élèves ORDER BY a_nais, m_nais, j_nais
```

| numéro| prénom       | nom      | j_nais | m_nais | a_nais | classe |
|-----------------|---------------|----------|-------------------|-------------------|--------------------|--------|
| 24362736        | Luna          | Petrov   | 20                | 4                 | 2005               | TGC    |
| 12345678        | Sofia         | Ramirez  | 5                 | 1                 | 2006               | TGA    |
| 23456789        | Malik         | Khan     | 12                | 3                 | 2006               | TGB    |
| 25746380        | Andrei        | Popescu  | 8                 | 7                 | 2006               | TGA    |
| 29038746        | Isabella      | Costa    | 15                | 9                 | 2006               | TGB    |
| 30948637        | Kai           | Nakamura | 3                 | 11                | 2006               | TGC    |


### Éléments distincts (DISTINCT)

Que se passe-t-il si l'on exécute la requête suivante ?

```sql 
SELECT classe FROM élèves
```

|classe|
|--|
|TGA|
|TGB|
|TGC|
|TGA|
|TGB|
|TGC|

Cette requête nous renvoie toute la colonne classe de la relation *élèves*. Si nous souhaitons éviter les doublons, il suffit de rajouter la clause **DISTINCT**

```sql 
SELECT DISTINCT classe FROM élèves
```

|classe|
|--|
|TGA|
|TGB|
|TGC|

## Jointure de tables

L'intérêt de définir une clé étrangère dans une relation est de pouvoir établir des liens entre les différentes relations. Prenons cette question : qui est le premier professeur principal de Sofia Ramirez ? D'après la relation *élèves*, Sofia est en classe de TGA, et selon la relation *classes*, le premier professeur principal de cette classe est Machin. Tout ceci est assez simple lorsque l'on a accès directement aux informations, mais il ne faut pas oublier que ce n'est pas le cas lorsque l'on exploite une base de données.

Pour recouper deux relations d'une base de données, on effectue une jointure. Celle-ci s'effectue avec la syntaxe **JOIN table ON correspondance**. Ici, on souhaite rassembler les relations *élèves* et *classes* : le critère de rassemblement se porte sur la colonne *classe* de la relations *élèves* et la colonne *nom_classe* de la relation *classes*.

```sql
SELECT *
FROM élèves
JOIN classes ON élèves.classe = classes.nom_classe
```

| numéro| prénom       | nom      | j_nais | m_nais | a_nais | classe |nom_classe|pp1|pp2|
|-----------------|---------------|----------|-------------------|-------------------|--------------------|--------|--|--|--|
| 12345678        | Sofia         | Ramirez  | 5                 | 1                 | 2006               | TGA    |TGA|Machin|Chouette|
| 25746380        | Andrei        | Popescu  | 8                 | 7                 | 2006               | TGA    | TGB|Bidule|Quidam|
| 24362736        | Luna          | Petrov   | 20                | 4                 | 2005               | TGC    |TGC|Chose|Exemple|
| 30948637        | Kai           | Nakamura | 3                 | 11                | 2006               | TGC    |TGA|Machin|Chouette|
| 23456789        | Malik         | Khan     | 12                | 3                 | 2006               | TGB    |TGB|Bidule|Quidam|
| 29038746        | Isabella      | Costa    | 15                | 9                 | 2006               | TGB    |TGC|Chose|Exemple|

Il est alors possible de combiner cette jointure avec ce que nous avons vu précédemment. On pourra par exemple afficher les prénoms, noms et premiers professeurs principaux correspondants à chaque ligne de cette jointure. Par souci de clarté et pour éviter toute confusion ou ambiguïté, on précisera la relation dont est issue l'attribut que nous souhaitons conserver.

```sql
SELECT élèves.prénom, élèves.nom, classes.pp1
FROM élèves
JOIN classes ON élèves.classe = classes.nom_classe
```

| prénom       | nom       |pp1|
|---------------|----------|-------------------
| Sofia         | Ramirez   |Machin|
| Andrei        | Popescu   |Bidule|
| Luna          | Petrov   |Chose|
| Kai           | Nakamura |Machin|
| Malik         | Khan     |Bidule|
| Isabella      | Costa    |Chose|

Il est alors possible d'utiliser une clause **WHERE** suite à cette jointure.

```sql
SELECT élèves.prénom, élèves.nom, classes.pp1
FROM élèves
JOIN classes ON élèves.classe = classes.nom_classe
WHERE élèves.nom = 'Ramirez'
```

| prénom       | nom       |pp1|
|---------------|----------|-------------------
| Sofia         | Ramirez   |Machin|


## Mise à jour et suppression

La mise à jour d'une relation se fait à l'aide du mot clé **UPDATE**. La clause **WHERE** permet de sélectionner les lignes à mettre à jour tandis que le mot-clé **SET** permet de désigner les mises à jour à faire. Par exemple, la requête suivante assignera Sofia Ramirez à la classe TGB.

```sql
UPDATE élèves
SET classe = 'TGB'
WHERE nom = 'Ramirez' AND prénom = 'Sofia'
```

On peut alors vérifier la mise à jour en affichant la table

```sql
SELECT * FROM élèves
```

| numéro| prénom       | nom      | j_nais | m_nais | a_nais | classe |
|-----------------|---------------|----------|-------------------|-------------------|--------------------|--------|
| 12345678        | Sofia         | Ramirez  | 5                 | 1                 | 2006               | TGB    |
| 23456789        | Malik         | Khan     | 12                | 3                 | 2006               | TGB    |
| 24362736        | Luna          | Petrov   | 20                | 4                 | 2005               | TGC    |
| 25746380        | Andrei        | Popescu  | 8                 | 7                 | 2006               | TGA    | 
| 29038746        | Isabella      | Costa    | 15                | 9                 | 2006               | TGB    |
| 30948637        | Kai           | Nakamura | 3                 | 11                | 2006               | TGC    |

Pour supprimer une ou plusieurs entrées, on utilise le mot-clé **DELETE**. Attention à bien préciser les conditions de suppression avec **WHERE**, sans quoi vous supprimerez tous les enregistrements de la table !

```sql
DELETE FROM élèves
WHERE nom = 'Ramirez' AND prénom = 'Sofia'
```

Là encore, on peut visualiser la mise à jour en affichant toute la table

```sql
SELECT * FROM élèves
```

| numéro| prénom       | nom      | j_nais | m_nais | a_nais | classe |
|-----------------|---------------|----------|-------------------|-------------------|--------------------|--------|
| 23456789        | Malik         | Khan     | 12                | 3                 | 2006               | TGB    |
| 24362736        | Luna          | Petrov   | 20                | 4                 | 2005               | TGC    |
| 25746380        | Andrei        | Popescu  | 8                 | 7                 | 2006               | TGA    | 
| 29038746        | Isabella      | Costa    | 15                | 9                 | 2006               | TGB    |
| 30948637        | Kai           | Nakamura | 3                 | 11                | 2006               | TGC    |

Rappelons par ailleurs qu'il n'est pas possible de supprimer un enregistrement si l'un de ses attributs est une clé étrangère d'une autre table et correspond à un enregistrement de cette table. Par exemple, la commande suivante renverra une erreur.

```sql
DELETE FROM classes
WHERE nom_classe = 'TGA'
```

En effet, certains enregistrements de la table *élèves* font référence à la ligne que l'on essaie de supprimer. Le SGBD indiquera une ```FOREIGN KEY constraint failed```.