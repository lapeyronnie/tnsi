---
author: Jason Lapeyronnie
title: Base de données relationnelles
---

## Relations

!!! abstract "Vocabulaire"

    Dans le modèle relationnel, les données sont stockées dans des tables, également appelés **Relations**

    - la première ligne de ce tableau constitue l'**entête** de la relation. Les éléments qui constituent l'entête s'appellent les **attributs** (ou **champs** ou encore **colonnes** ) de la relation.
    - le **domaine** de chaque attribut désigne l'ensemble des valeurs qui peuvent être prises par cet attribut.
    - chaque ligne de cette relation est appelé **enregistrement** ou **n-uplet**.

Pour bien comprendre, rien ne vaut un exemple ! Voici une relation nommée *élèves* un extrait d'une base de données fictives d'élèves (générée par ce formidable outil qu'est chatGPT).


| Numéro étudiant | Prénom        | Nom       | Jour de naissance | Mois de naissance | Année de naissance | Classe | Professeur principal 1 | Professeur principal 2 |
|-----------------|---------------|----------|-------------------|-------------------|--------------------|--------|------------------------|------------------------|
| 12345678        | Sofia         | Ramirez  | 5                 | 1                 | 2006               | TGA    | M. Machin              | Mme Chouette           |
| 23456789        | Malik         | Khan     | 12                | 3                 | 2006               | TGB    | Mme Bidule             | M. Quidam              |
| 24362736        | Luna          | Petrov   | 20                | 4                 | 2005               | TGC    | Mme Chose              | Mme Exemple            |
| 25746380        | Andrei        | Popescu  | 8                 | 7                 | 2006               | TGA    | M. Machin              | Mme Chouette           |
| 29038746       | Isabella      | Costa    | 15                | 9                 | 2006               | TGB    | Mme Bidule             | M. Quidam              |
| 30948637        | Kai           | Nakamura | 3                 | 11                | 2006               | TGC    | Mme Chose              | Mme Exemple            |


Sur cet exemple...

- Le premier enregistrement contient les éléments 12345678, Sofia, Ramirez, 5, 1, 2006, TGA, M. Machin, Mme Chouette.
- L'attribut *Nom* contient les éléments Ramirez, Khan, Petrov, Popescu, Costa, Nakamura.
- Le domaine de l'attribut *Nom* est un texte, celui de l'attribut *Jour de Naissance* est un entier

Selon le système de gestion de base de données utilisé, les domaines possibles seront plus ou moins restreints. Par exemple, si l'on utilise SQLite, les domaines possibles sont NULL, INT (entier signé sur au plus 8 octets), REAL (nombre à virgule flottante), TEXT ou BLOB (valeur binaire brute).

D'autres formats sont possibles si l'on utilise d'autres SGBD. On peut par exemple citer les domaines DATE, TIME, CHAR pour des chaînes de caractère de taille fixée (en octet, et pas en nombre de caractères !) ou VARCHAR pour des chaînes de caractère de taille variable. Par exemple, l'attribut *Nom* pourrait avoir un domaine VARCHAR(50) (il existe sans doute très peu de noms qui demandent plus de 50 octets pour être enregistrés).


## Clés primaires 

Dans une base de donnnés, il est impossible d'avoir deux enresgistrements identiques. Ajouter une nouvelle fois l'enregistrement 12345678, Sofia, Ramirez, 5, 1, 2006, TGA, M. Machin, Mme Chouette provoquera une erreur, puisque celui-ci figure déjà dans notre table.

Plutôt que de vérifier tous les attributs pour chaque enregistrement, nous allons en sélectionner certains qui nous permettront d'identifier de manière unique un enregistrement dans une base de données : il s'agit de la clé primaire.

!!! abstract "Clé primaire"

    Dans une relation, la clé primaire est un attribut (ou un ensemble d'attributs) qui identifie de manière unique un enregistrement de la table.

Dans notre table *élèves*, l'attribut *Numéro étudiant* peut faire office de clé primaire : à chaque élève est associée un unique numéro étudiant, et réciproquement. C'est d'ailleurs tout l'intérêt de ce numéro !

En revanche, l'attribut *Nom* ne pourra pas jouer le rôle de clé primaire : il se peut que plusieurs élèves de notre base de données portent le même nom.


## Clés étrangères

Vous avez peut-être remarqué que notre relation *élèves* comportaient certaines répétitions : en effet, tous les élèves recensés en classe de TGA auront comme professeurs principaux M. Machin et Mme Chouette.

Cette redondance des données est en règle générale une très mauvaise chose dans une base de données. Aussi est-il souhaitable de ne conserver que la classe de nos élèves dans notre relation *élèves* et de créer une nouvelle relation *classes* qui comportera les informations relatives à la classe, et non à l'élève. Nous nous retrouvons donc avec les deux relations suivantes.

Relation **élèves**

| Numéro étudiant | Prénom        | Nom       | Jour de naissance | Mois de naissance | Année de naissance | Classe |
|-----------------|---------------|----------|-------------------|-------------------|--------------------|--------|
| 12345678        | Sofia         | Ramirez  | 5                 | 1                 | 2006               | TGA    |
| 23456789        | Malik         | Khan     | 12                | 3                 | 2006               | TGB    |
| 24362736        | Luna          | Petrov   | 20                | 4                 | 2005               | TGC    |
| 25746380        | Andrei        | Popescu  | 8                 | 7                 | 2006               | TGA    | 
| 29038746       | Isabella      | Costa    | 15                | 9                 | 2006               | TGB    |
| 30948637        | Kai           | Nakamura | 3                 | 11                | 2006               | TGC    |

Relation **classes**

| Nom de la classe | Professeur principal 1 | Professeur principal 2 |
|-----------------|---------------|----------|
| TGA    | M. Machin              | Mme Chouette           |
| TGB    | Mme Bidule             | M. Quidam              |
| TGC    | Mme Chose              | Mme Exemple            |

Dans cette nouvelle relation, l'attribut *Nom de la classe* fait office de clé primaire (il n'est pas obligatoire d'avoir une clé primaire numérique, même si cela est vivement conseillé). Dans la relation *élèves*, l'attribut *Classe* fait alors référence à l'attribut *Nom de la classe* qui est la clé primaire de la relation *classes* : on parle alors de **clé étrangère**.

!!!abstract "Clé étrangère"

    On considère deux relations R1 et R2.

    Une **clé étrangère** de la relation R1 est un attribut dont les valeurs sont celles prises par la clé primaire de la relation R2.

    Lorsqu'un enregistrement de la relation R1 fait référence à une information de la relation R2, le SGBD s'assure que cette dernière information existe bel et bien dans la relation R2 : on parle alors d'**intégrité référentielle**.

La notion de clé étrangère, en plus de permettre d'établir un lien entre les différentes relations d'une base de données, permet également de s'assurer de l'intégrité des données : il n'est pas possible, par exemple, d'intégrer à la relation *élèves* un enregistrement dont la classe serait *TGD* si cette même classe ne figure pas dans la relation *classes*. Ceci suppose donc que la relation *classes* a été construite **avant** la relation *élèves*.

De manière similaire, il n'est pas possible de supprimer une classe de la relation *classes* si au moins un élève est assigné à cette classe : supprimer l'enregistrement (TGC, Mme Chose, Mme Exemple) est impossible puisque le troisième enregistrement de la relation *élèves* fait référence à la classe TGC.

Il serait possible d'aller plus loin dans l'élaboration de notre base de données. Par exemple, on pourrait imaginer une relation *professeurs* dont la clé primaire serait un entier *id_professeur* et qui comporterait toutes les informations sur les professeurs du lycée. Dans la relation *classes*, les noms des professeurs principaux seraient alors remplacés par les identifiants des professeurs concernés, ce qui créerait de fait deux clés étrangères dans la relation *classes*.

## Schéma relationnel

Naturellement, ces raisonnements sont à effectuer en amont de la création et de la complétion de la base de données. Ces informations doivent être établies au préalable à l'aide d'un **schéma relationnel**

!!!abstract "Schéma relationnel"

    Un schéma relationnel présente l'ensemble des relations d'une base de données relationnelle.

    Dans ce schéma, il est nécessaire de présenter plusieurs informations
    
    - l'ensemble des attributs de chaque relation ;
    - les domaines de chacun de ces attributs ;
    - les clés primaires et éventuelles clés étrangères de chaque relation.


Dans notre exemple, nous pouvons établir le schéma relationnel suivant. Pour plus de simplicité, nous renommons par ailleurs les attributs de ces relations. Les clés primaires sont soulignées et les clés étrangères sont précédées du symbole #.

- professeurs(<u>id</u> : INT, prénom : TEXT, nom : TEXT)
- classes(<u>nom</u> : TEXT, #pp1 : INT, #pp2 : INT)
- élèves(<u>id</u> : INT, prénom : TEXT, nom : TEXT, j_nais : INT, m_nais : INT, a_nais : INT, #classe : TEXT)

Ce schéma relationnel peut également être présenté sous la forme d'un diagramme.

![Diagramme relationnel](diagramme_relation.png){margin:auto;}