---
author: Jason Lapeyronnie
title: Exercices - SQL
---

## Métropole 2022, sujet 1

On pourra utiliser les mots clés SQL suivants : 

**SELECT, FROM, WHERE, JOIN, ON, INSERT, INTO, VALUES, UPDATE, SET, AND**

Nous allons étudier une base de données traitant du cinéma dont voici le schéma
relationnel qui comporte 3 relations 

- la relation **individu** (<u>id_ind</u>, nom, prenom, naissance)
- la relation **realisation** (<u>id_rea</u>, titre, annee, type)
- la relation **emploi** (<u>id_emp</u>, description, #id_ind, #id_rea)

Les clés primaires sont soulignées et les clés étrangères sont précédées d’un #.  
Ainsi emploi.id_ind est une clé étrangère faisant référence à individu.id_ind.  
Voici un extrait des tables individu et realisation :

Extrait de **individu**

|id_ind|nom|prenom|naissance|
|--|--|--|--|
|105|'Hulka'|'Daniel'|'01-06-1968'|
|403|'Travis'|'Daniel'|'10-03-1968'|
|688|'Crog'|'Daniel'|'07-07-1968'|
|695|'Pollock'|'Daniel'|'24-08-1968'|

Extrait de **realisation**

|id_rea|titre|annee|type|
|--|--|--|--|
|105|'Casino Imperial'|2006|'action'|
|325|'Ciel tombant'|2012|'action'|
|655|'Fantôme'|2015|'action'|
|950|'Mourir pour attendre'|2021|'action'|

!!! note "Questions"

    1. On s’intéresse ici à la récupération de données dans une relation.
        * Écrire ce que renvoie la requête ci-dessous :

        ```sql
        SELECT nom, prenom, naissance
        FROM individu
        WHERE nom = 'Crog';
        ```

        * Fournir une requête SQL permettant de récupérer le titre et la clé primaire de chaque film dont la date de sortie est strictement supérieure à 2020.
    2. Cette question traite de la modification de relations.
        * Dire s’il faut utiliser la requête 1 ou la requête 2 proposées ci-dessous pour modifier la date de naissance de Daniel Crog. Justifier votre réponse en expliquant pourquoi la requête refusée ne pourra pas fonctionner.

        ```sql
        -- Requête 1
        UPDATE individu
        SET naissance = '02-03-1968'
        WHERE id_ind = 688 AND nom = 'Crog' AND prenom = 'Daniel';
        ```

        ```sql
        -- Requête 2
        INSERT INTO individu
        VALUES (688, 'Crog', 'Daniel', '02-03-1968');
        ```

        * Expliquer si la relation *individu* peut accepter (ou pas) deux individus portant le même nom, le même prénom et la même date de naissance.
    3. Cette question porte sur la notion de clés étrangères.
        * Compléter les demandes ci-dessous pour qu’elles ajoutent dans la relation *emploi* les rôles de Daniel Crog en tant que James Bond dans le film nommé ‘Casino Impérial’ puis dans le film ‘Ciel tombant’.

        ```sql
        INSERT INTO emploi
        VALUES (5400, 'Acteur(James Bond)', ... );

        INSERT INTO emploi
        VALUES (5401, 'Acteur(James Bond)', ... );
        ```

        * On désire rajouter un nouvel emploi de Daniel Crog en tant que James Bond dans le film 'Docteur Yes'. Expliquer si l’on doit d’abord créer l’enregistrement du film dans la relation *realisation* ou si l’on doit d’abord créer le rôle dans la relation *emploi*.
    4. Cette question traite des jointures.
        * Compléter la requête SQL ci-dessous de façon à ce qu’elle renvoie le nom de l’acteur, le titre du film et l’année de sortie du film, à partir de tous les enregistrements de la relation emploi pour lesquels la description de l’emploi est 'Acteur(James Bond)'.

        ```sql
        SELECT ...
        FROM emploi
        JOIN individu ON ...
        JOIN realisation ON ...
        WHERE emploi.description = 'Acteur(James Bond)';
        ```

        * Fournir une requête SQL permettant de trouver toutes les descriptions des emplois de Denis Johnson (Denis est son prénom et Johnson est son nom). On veillera à n’afficher que la description des emplois et non les films associés à ces emplois.

??? success "Solution"

    1a. La requête renvoie 'Crog', 'Daniel', '07-07-1968'

    1b. La requête à utiliser est

    ```sql
    SELECT titre, id_rea
    FROM realisation
    WHERE annee > 2020
    ```

    2a. Il faut utiliser la première requête pour mettre à jour la table. La deuxième relation tentera d'insérer une nouvelle ligne mais sera refusée puisqu'un enregistrement ayant la clé primaire 688 est déjà enregistrée dans la table *individu*.

    2b. La relation *individu* peut accepter deux individus ayant même nom, prénom et date de naissance, à condition que les clés primaires associées à ces individus ne figurent pas déjà dans la relation.

    3a. Daniel Crog a l'identifiant 688. Les films 'Casino Imperial' et 'Ciel Tombant' ont respectivement pour identifiant 105 et 325. On entre donc la requête suivante :

    ```sql
    INSERT INTO emploi
    VALUES (5400, 'Acteur(James Bond)', 688, 105 );

    INSERT INTO emploi
    VALUES (5401, 'Acteur(James Bond)', 688, 325 );
    ```

    3b. Il faut d'abord créer l'enregistrement dans la relation *ralisation* afin de pouvoir faire référence à ce film en ajoutant l'enregistrement correspondant dans la relation *emploi*.

    4a. La requête à indiquer est

    ```sql
    SELECT individu.nom, realisation.titre, realisation.annee
    FROM emploi
    JOIN individu ON individu.id_ind= emploi.id_ind
    JOIN realisation ON realisation.id_rea = emploi.id_rea
    WHERE emploi.description = 'Acteur(James Bond)';
    ```

    4b. La requête à indiquer est

    ```sql
    SELECT emploi.description
    FROM emploi
    JOIN individu ON individu.id_ind= emploi.id_ind
    WHERE indidivu.nom = 'Johnson' AND individu.prenom = 'Denis' ;
    ```


## Centres étrangers, groupe 1 2023, sujet 1

L’énoncé de cet exercice utilise les mots clefs du langage SQL suivants : 

**SELECT, FROM, WHERE, JOIN...ON, UPDATE...SET, DELETE, INSERT INTO...VALUES, ORDER BY**

La clause **ORDER BY** suivie d'un attribut permet de trier les résultats par ordre croissant des valeurs de l'attribut.

Radio France souhaite créer une base de données relationnelle contenant les podcasts des émissions de radio. Pour cela elle utilise le langage SQL. Elle crée :

- une relation (ou table) *podcast* qui contient le thème et l’année de diffusion.
- une relation *emission* qui contient les émissions (id_emission, nom), la radio de diffusion et l’animateur.
- une relation *description* qui contient un résumé et la durée du podcast en minutes.


Relation **podcast**

|id_podcast |theme |annee |id_emission|
|--|--|--|--|
|1 |Le système d’enseignement supérieur français est-il juste et efficace ? |2022 |10081|
|2 |Trois innovations pour la croissance future (1/3) : La révolution blockchain.| 2021 |10081|
|3 |Travailleurs de plateformes : vers un nouveau prolétariat ?| 2021| 10175|
|4| Le poids de la souveraineté numérique française |2019| 10183|
|40| Le poids de la souveraineté numérique française |2019| 10183|
|5| Dans le cloud en Islande, terre des data center| 2019 |10212|

Relation **emission**

|id_emission| nom |radio| animateur|
|--|--|--|--|
|10081 |Entendez-vous l’éco ? |France culture |Tiphaine De R.|
|10175| Le Temps du débat| France culture| Léa S.|
|10183| Soft power| France culture |Frédéric M.|
|10212 |La tête au carré| France inter| Mathieu V.|

*id_podcast* de la relation **podcast** et *id_emission* de la relation **emission** sont des clés primaires. L’attribut *id_emission* de la relation **podcast** fait directement référence à la clé primaire de la relation emission.

Relation **description**

|id_description| resume| duree| id_emission|
|--|--|--|--|
|101|Autrefois réservé à une élite, l’enseignement supérieur français s’est profondément démocratisé : donne-t-il pour autant les mêmes chances à chacun ?| 4 |10081|
|102| Quelles sont leurs conditions de travail et quels sont leurs moyens de contestation ? |58| 10175|
|103| La promesse de la blockchain, c’est la suppression des intermédiaires et la confiance à grande échelle.| 4| 10081|

!!! note "Questions"

    1 - Écrire le schéma relationnel de la relation **description**, en précisant les attributs et leurs types probables, la clé primaire et la ou les clé(s) étrangère(s) éventuelle(s).

    2a - Écrire ce qu’affiche la requête suivante appliquée aux extraits précédents :

    ```sql
    SELECT theme, annee FROM podcast WHERE id_emission = 10081
    ```

    2b - Écrire une requête SQL permettant d'afficher les thèmes des podcasts de l'année 2019.

    2c -  Écrire une requête SQL affichant la liste des thèmes et des années de diffusion des podcasts dans l'ordre chronologique des années.

    3a - Décrire simplement le résultat obtenu avec cette requête SQL.

    ```sql
    SELECT DISTINCT theme FROM podcast
    ```

    3b - Écrire une requête SQL supprimant la ligne contenant l'id_podcast = 40 de la relation **podcast**. 

    4a - Une erreur de saisie a été faite dans la relation **emission**. Écrire une requête SQL permettant de changer le nom de l’animateur de l'émission "Le Temps dudébat" en "Emmanuel L.".

    4b - Écrire une requête SQL permettant d'ajouter l'émission "Hashtag" sur la radio "France inter" avec "Mathieu V.". On lui donnera un id_emission égal à 12850.

    5 - Écrire une requête permettant de lister les thèmes, le nom des émissions et le résumé des podcasts pour lesquels la durée est strictement inférieure à 5 minutes

## Centres étrangers, groupe 1 2023, sujet 2

L’énoncé de cet exercice utilise les mots-clés du langage SQL suivants : 

**SELECT, FROM, WHERE, JOIN...ON, UPDATE...SET, INSERT INTO...VALUES..., COUNT, ORDER BY**

La clause **ORDER BY** suivie d'un attribut permet de trier les résultats par ordre croissant de l'attribut.

**SELECT COUNT(*)** renvoie le nombre de lignes d'une requête.

Un zoo souhaite pouvoir suivre ses animaux et ses enclos. Tous les représentants d’une espèce sont réunis dans un même enclos. Plusieurs espèces, si elles peuvent cohabiter ensemble, pourront partager le même enclos.

Il crée une base de données utilisant le langage SQL avec une relation (ou table) **animal** qui recense chaque animal du zoo. Vous trouverez un extrait de cette relation ci-dessous (les unités des attributs age, taille et poids sont respectivement ans, m et kg) :


Relation **animal**

|id_animal| nom| age| taille| poids |nom_espece|
|--|--|--|--|--|--|
|145| Romy| 18| 2.3 |130| tigre du Bengale|
|52|Boris| 30| 1.10| 48| bonobo|
|... |...| ... |... |...| ...|
|225| Hervé| 10 |2.4 |130| lama|
|404| Moris| 6| 1.70 |100 |panda|
|678| Léon |4 |0.30| 1| varan|

Il crée la relation **enclos** dont vous trouverez un extrait ci-dessous (l’unité de l’attribut surface est m²) :

Relation **enclos**

|num_enclos| ecosysteme |surface| struct |date_entretien|
|--|--|--|--|--|
|40 |banquise| 50 |bassin |04/12/2024|
|18| forêt tropicale |200 |vitré| 05/12/2024|
|...| ...| ...| ...| ...|
|24| savane| 300| clôture| 04/12/2024|
|68| désert |2| vivarium |05/12/2024|

Il crée également la relation **espece** dont vous trouverez un extrait ci-dessous :

Relation **espece**

|nom_espece |classe |alimentation| num_enclos|
|--|--|--|--|
|impala| mammifères |herbivore| 15|
|ara de Buffon |oiseaux |granivore| 77|
|... |...| ...| ...|
|tigre du Bengale| mammifères |carnivore| 18|
|caïman |reptiles |carnivore |45|
|manchot|empereur| oiseaux| carnivore| 40|
|lama| mammifères |herbivore |13|

!!! note "Questions"

    Cette question porte sur la lecture et l’écriture de requêtes SQL simples.

    **1a -** Écrire le résultat de la requête ci-dessous.

    ```sql
    SELECT age, taille, poids FROM animal WHERE nom = 'Moris';
    ```

    **1b -** Écrire une requête qui permet d’obtenir le nom et l’âge de tous les animaux de l’espèce bonobo, triés du plus jeune au plus vieux.

    Cette question porte sur le schéma relationnel.

    **2a -** Citer, en justifiant, la clé primaire et la clé étrangère de la relation espece.

    **2b -** Donner le modèle relationnel de la base de données du zoo. On soulignera les clés primaires et on fera précéder les clés étrangères d’un #.

    Cette question porte sur les modifications d’une table. 

    **3a -** L’espèce ornithorynque a été entrée dans la base comme étant de la classe des oiseaux alors qu’il s’agit d’un mammifère. Écrire une requête qui corrige cette erreur dans la table espece.

    **3b -** Le couple de lamas du zoo vient de donner naissance au petit lama nommé "Serge" qui mesure 80 cm et pèse 30 kg. Écrire une requête qui permet d’enregistrer ce nouveau venu au zoo dans la base de données, sachant que les clés primaires de 1 à 178 sont déjà utilisées.

    Cette question porte sur la jointure entre deux tables

    **4a -** Compléter les ... de la requête SQL suivante afin de recenser le nom et l’espèce de tous les animaux carnivores vivant en vivarium dans le zoo.

    ```sql
    SELECT ...
    FROM animal
    JOIN espece ON ...
    JOIN enclos ON ...
    WHERE enclos.struct = 'vivarium' and ...;
    ```

    **4b - **On souhaite connaître le nombre d’animaux dans le zoo qui font partie de la classe des oiseaux. Écrire la requête qui permet de compter le nombre d’oiseaux dans tout le zoo.


## Polynésie 2022, sujet 1

L’énoncé de cet exercice peut utiliser les mots du langage SQL suivants :

**CREATE TABLE, SELECT, FROM, WHERE, JOIN ON, INSERT INTO, VALUES, UPDATE, SET, DELETE, COUNT, DISTINCT, AND, OR, AS, ORDER BY, ASC, DESC**

Un site web recueille des données de navigation dans une base de données afin d'étudier les profils de ses visiteurs.  
Chaque requête d'interrogation d'une page de ce site est enregistrée dans une première table dénommée *Visites sous* la forme d'un 5-uplet : (identifiant, adresse IP, date et heure de visite, nom de la page, navigateur).

Le chargement de la page index.html par 192.168.1.91 le 12 juillet 1998 à 22h48 aura par exemple été enregistré de la façon suivante :  
(1534, "192.168.1.91", "1998-07-12 22:48:00", "index.html", "Internet explorer 4.1").

La commande SQL ayant permis de créer cette table est la suivante:

```sql
CREATE TABLE Visites (
identifiant INTEGER NOT NULL UNIQUE,
ip VARCHAR(15),
dateheure DATETIME,
nompage TEXT,
navigateur TEXT
);
```

!!! note "Question 1"

    1. Donner une commande d'interrogation en langage SQL permettant d'obtenir l'ensemble des 2-uplets (adresse IP, nom de la page) de cette table.
    2. Donner une commande en langage SQL permettant d'obtenir l'ensemble des adresses IP ayant interrogé le site, sans doublon.
    3. Donner une commande en langage SQL permettant d'obtenir la liste des noms des pages visitées par l'adresse IP 192.168.1.91

Ce site web met en place, sur chacune de ses pages, un programme en javascript qui envoie au serveur, à intervalle régulier de 15 secondes, le temps en secondes de présence sur la page. Ces envois contiennent tous la valeur de identifiant correspondant au chargement initial de la page.

Par exemple, si le visiteur du 12 juillet 1998 est resté 65 secondes sur la page, celle-ci a envoyé au serveur les 4 doublets (1534, 15), (1534, 30), (1534, 45) et (1534, 60).


Ces données sont enregistrées dans une table nommée *Pings* créée avec la commande ci-dessous :

```sql
CREATE TABLE Pings (
identifiant INTEGER,
duree INTEGER
);
```

En plus de l'inscription d'une ligne dans la table *Visites*, chaque chargement d'une nouvelle page provoque l'insertion d'une ligne dans la table *Pings* comprenant l'identifiant de ce chargement et une durée de 0.

Les attributs identifiant des tables Visites et Pings partagent les mêmes valeurs.

!!! note "Question 2"

    1. De quelle table l’attribut identifiant est-il la clé primaire ?
    2. De quelle table l’attribut identifiant est-il une clé étrangère ?
    3. Par conséquent, quelles vérifications sont automatiquement effectuées par le système de gestion de base de données ?

!!! note "Question 3"

    Le serveur reçoit le doublet (identifiant, duree) suivant : (1534, 105). Écrire la commande SQL d'insertion qui permet d'ajouter cet enregistrement à la
    table *Pings*.

On envisage ensuite d'optimiser la table en se contentant d'une seule ligne par identifiant dans la table *Pings* : les valeurs de l'attribut duree devraient alors être mises à jour à chaque réception d'un nouveau doublet (identifiant, duree).
   
!!! note "Question 4"    
    
    1. Écrire la requête de mise à jour permettant de fixer à 120 la valeur de l'attribut duree associée à l'identifiant 1534 dans la table Pings.
    2. Expliquer pourquoi on ne peut pas être certain que les données envoyées par une page web, depuis le navigateur d'un client, via plusieurs requêtes formulées en javascript, arrivent au serveur dans l'ordre dans lequel elles ont été émises.
    3. En déduire qu'il est préférable d'utiliser une requête d'insertion plutôt qu'une requête de mise à jour pour ajouter des données à la table Pings.

!!! note "Question 5"

    Écrire une requête SQL utilisant le mot-clef **JOIN** et une clause **WHERE**, permettant de trouver les noms de toutes les pages qui ont été consultées plus d'une minute par au moins un utilisateur.
