---
author: Jason Lapeyronnie
title: Exercices - BDD
---

## Métropole 2023, sujet 1

Un grand magasin de meubles propose à ses clients un large choix de meubles. Les
informations correspondantes sont rangées dans une base de données composée de
trois relations.

Voici le schéma de deux de ces relations :

- *Clients* (<u>id</u>, nom, prenom, adresse, ville)
- *Commandes* (<u>id</u>, #idClient, #idMeuble, quantite, date)

Dans ce schéma :

- la clé primaire de chaque relation est définie par les attributs soulignés ;
- les attributs précédés de # sont les clés étrangères.

La troisième relation est appelée *Meubles* et concerne les meubles du magasin.
Le tableau de la figure ci-dessous en présente un extrait :

|id| intitule| prix| stock| description|
|--|--|--|--|--|
|62|'skap'|69.99|2|'Armoire blanche 3 portes'|
|63|'skap'|69.99|3|'Armoire noire 3 portes'|
|74|'stol'|39.99|10|'Chaise en bois avec tissu bleu'|
|98|'hylla'|99.99|0|'Bibliothèque 5 étages blanche'|

1. Donner la caractéristique qu’un attribut doit avoir pour être choisi comme clé primaire.
2. Expliquer le rôle des deux clés étrangères de la relation Commandes.
3. Donner le schéma relationnel de la relation Meubles en précisant la clé primaire et les éventuelles clés étrangères. Préciser le domaine de chaque attribut.

## Amérique du Nord 2022, sujet 2

Une salle de cinéma propose un site Web à ses abonnés afin d'effectuer des réservations de séances en ligne. Deux tarifs sont proposés : plein et réduit (-16 ans, senior +65 ans, étudiant...). Le site est associé à une base de données dont le modèle relationnel contient les quatre relations décrites ci-dessous.

![Schéma relationnel](schema_bdd.PNG)

Un attribut souligné correspond à une clé primaire et un attribut précédé du symbole # correspond à une clé étrangère.

Voici un extrait de quelques enregistrements des relations *Films*, *Séance* et *Abonné*

- Extrait de la relation *Films*, les durées sont en minutes :

|idFilm|titre|réalisateur|durée|
|--|--|--|--|
|1|Le sens de la famille|Jean-Patrick Benes|90|
|2|Les croods 2| Joel Crawford|95|
|8|Black Widow| Cate Shortland | 134|

- Extrait de la relation *Séance*, les dates sont au format année-mois-jour :

|idSéance|idFilm|date|heure|
|--|--|--|--|
|35|1|2021-10-11|21:00|
|737|8|2021-10-11|21:00|
|738|8|2021-10-13|16:15|

Extrait de la relation *Abonné* :

|idAbonné|nom|prénom|mail|
|--|--|--|--|
|1|Henry|Jean|jean.henry@envoi.fr|
|2|Jacquin|Morgane|jacquin.morgane@mail.fr|
|13|Dupont|Charles|charles.dupont@envoi.fr|

1. Définir le rôle d'une clé primaire
2. Définir le rôle d'une clé étrangère
3. Déterminer, en justifiant, si un abonné peut réserver plusieurs fois une même séance.
3. M. Charles Dupont réserve 3 places au tarif plein et deux places au tarif réduit pour assister à la projection du film "Black Widow" le 11 octobre 2021 à 21:00. A l'aide des extraites des relations données précédemment, recopier et compléter l'enregistrement correspondant dans la relation *Réservation* ci-dessous :

|idResérvation|idAbonné|idSéance|nbPlaces_plein|nbPlaces_reduit|
|--|--|--|--|--|
|243|...|...|...|...|



## Métropole 2022, sujet 2

Un musicien souhaite créer une base de données relationnelle contenant ses
morceaux et interprètes préférés. Il crée une table
*morceaux* qui contient entre autres les titres des morceaux et leur année de sortie 

|id_morceau | titre |annee |id_interprete|
|--|--|--|--|
|1| Like a Rolling Stone| 1965| 1|
|2| Respect| 1967| 2|
|3| Imagine| 1970| 3|
|4 |Hey Jude |1968| 4|
|5| Smells Like Teen Spirit |1991| 5|
|6| I Want To hold Your Hand |1963 |4|

Il crée la table *interpretes* qui contient les interprètes et leur pays d'origine :

|id_interprete |nom| pays|
|--|--|--|
|1| Bob Dylan| États-Unis|
|2 |Aretha Franklin| États-Unis|
|3 |John Lennon |Angleterre|
|4 ||The Beatles |Angleterre|
|5 |Nirvana| États-Unis|


*id_morceau* de la table *morceaux* et *id_interprete* de la table *interpretes*
sont des clés primaires.

L’attribut *id_interprete* de la table *morceaux* fait directement référence à la clé
primaire de la table *interpretes*.

1. Citer, en justifiant, la clé étrangère de la table *morceaux*.
2. Écrire un schéma relationnel des tables *interpretes* et *morceaux*.

## Polynésie 2023, sujet 2

Un site permet à ses membres de proposer à la location du matériel et de louer du matériel. Ceci permet de mutualiser du matériel entre membres et au propriétaire de rentabiliser cet achat. Le temps d'utilisation du matériel s'en trouve ainsi augmenté et le nombre d'appareils diminué.

Le modèle relationnel est donné par le schéma ci-dessous. La table *Membre* contient les informations de chaque utilisateur du site (nom, prénom et code postal). La table *Objet* décrit le type d'objet à la location ainsi que son tarif de location journalier. La table *Reservation* répertorie toutes les réservations effectuées par les membres du site avec notamment leur date de début et de fin de location. La table *Possede* permet
de lier les tables Membre et Objet.

![Schéma relationnel](bddpoly2023s2.png)

On donne ci-dessous le contenu de ces tables à un instant donné :

Table *Membre*

|id_membre|nom|prenom|cp|
|--|--|--|--|
|1|Ali|Mohamed|69110|
|2|Alonso|Fernando|69005|
|3|Dupont|Antoine|69003|
|4|Ferrand|Pauline|69160|
|5|Kane|Harry|69003|

Table *Objet*

|id_objet|description|tarif|
|--|--|--|
|1|Nettoyeur haute pression|20|
|2|Taille-haie|15|
|3|Perforatrice|15|
|4|Appareil à raclette|10|
|5|Scie circulaire|15|
|6|Appareil à gaufre|10|

Table *Reservation*

|id_reservation |id_objet| id_membre |date_location| date_retour|
|--|--|--|--|--|
|1| 4 |5| 2022-02-18| 2022-02-19|
|2| 1 |2| 2022-05-05| 2022-05-06|
|3| 3 |1| 2022-07-10| 2022-07-12|
|4| 3 |1| 2022-08-12| 2022-08-14|
|5| 2 |2| 2022-10-20| 2022-10-22|
|6| 2 |2| 2022-10-20| 2022-10-22|

Table *Possede*

|id_membre|id_objet|
|--|--|
|1|4|
|1|6|
|2|4|
|3|3|
|3|5|
|4|1|
|4|2|

1.  Indiquer quels sont les prénoms et noms du ou des membres du site qui proposent la location d’un appareil à raclette ;
2. Donner le prénom et le nom du membre qui ne propose pas d’objet à la location.
3. Identifier le prénom et nom de l'emprunteur, du prêteur, ainsi que l'objet prêté lors de la réservation dont l'identifiant est 6.
4. Expliquer la limitation importante d’utilisation du service offert par le site si l’on utilisait le couple de clés étrangères (id_objet, id_membre) en tant que clé primaire de la relation *Reservation*.
5. Mohamed Ali décide de ne plus être membre du site. Il faut donc le supprimer de la table *Membre*. Expliquer pourquoi cette simple suppression est impossible en l'état.