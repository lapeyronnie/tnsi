---
author: Jason Lapeyronnie
title: Base de données - Introduction
---

## Retour sur le format CSV

L'année dernière, votre travail sur les données structurées en tables s'est notamment faite au moyen de fichiers CSV. Pour rappel, les fichiers au format CSV ressemblaient à ceci

```
nom,prénom,date_naissance
Lovelace,Ada,10/12/1815
Turing,Alan,23/06/1912
Knuth,Donald,10/01/1938
```

Ce fichier pouvait alors être visualisé sous la forme d'un tableau

|**nom**|**prénom**|**date_naissance**|
|--|--|--|
|Lovelace|Ada|10/12/1815|
|Turing|Alan|23/06/1912|
|Knuth|Donald|10/01/1938|

L'étude de ces fichiers passait par exemple par la création d'une liste de dictionnaires dont les clés étaient les noms des colonnes (également appelés *descripteurs*) et dont les valeurs correspondaient aux informations données sur chaque ligne. Dans notre cas, cela pourrait aboutir à la liste suivante :

```python
tab = [{'nom' : 'Lovelace', 'prénom' : 'Ada', 'date_naissance' : '10/12/1815'},
       {'nom' : 'Turing', 'prénom' : 'Alan', 'date_naissance' : '23/06/1912'},
       {'nom' : 'Lovelace', 'prénom' : 'Ada', 'date_naissance' : '10/01/1938'} ]
```

Le format CSV est facile à mettre en oeuvre et à étudier. Seulement, celui-ci n'est pas adapté au traitement d'un très grand volume de données, en particulier lorsque ces données sont réparties dans plusieurs fichiers qu'il faut alors recouper et actualiser.

## Bases de données

!!! abstract "Résumé"

    Une base de données (BDD) permet de stocker efficacement un grand volume d'informations. Les premières bases de données apparaissent dans les années 1960 et leur utilisation a totalement explosé depuis.

    Il existe plusieurs types de bases de données, notons par exemple...

    - les bases de données *relationnelles*, dans lesquelles les informations sont stockées sous la forme de tableau à deux dimensions
    - les bases de données *non relationnelles*, également appelées , qui utilisent des données de formes plus variées.

Les données des BDD sont stockés dans des fichiers qu'il n'est pas possible de modifier directement, en brut, comme on pourrait le faire avec un fichier CSV et un logiciel comme Excel ou LibreOfficeCalc. 

Les logiciels permettant d'écrire, lire et modifier le contenu d'une base de données s'appellent les **SGBD** (Système de Gestion de Base de Données). En outre, ces SGBD permettent de gérer les droits d'accès aux données au différents utilisateurs de cette base et d'assurer la sécurité de ces données, y compris lorsque plusieurs utilisateurs y accèdent et les tentent de les modifier.

Parmi les SGBD les plus connus, on compte des logiciels libres comme mySQL ou postgreSQL, ou des logiciels propriétaires, comme Oracle Database, Microsoft SQL Server ou IBM DB2.

