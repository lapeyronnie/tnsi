---
author: Jason LAPEYRONNIE
title: Sécurisation des communications
---

## Notion de chiffrement

Supposons que deux individus, Alice et Bob, souhaitent communiquer, par exemple par l'intermédiaire d'un réseau informatique. Alice et Bob désirent toutefois qu'une tierce personne soit incapable de lire leurs messages si d'aventure ceux-ci venaient à être interceptés. Pour cela, Alice et Bob vont chiffrer leur message, c'est-à-dire qu'ils vont le transformer en un message inintelligible pour toute personne qui n'aura pas les moyens de déchiffrer ce message. Pour chiffrer ou déchiffrer un message, on utilise un clé, qui peut être selon les cas, publique ou privée.

## Chiffrement symétrique

!!! abstract "Chiffrement symétrique"

    Une méthode de chiffrement est dite symétrique (ou à clé symétrique) si la même clé permet à la fois de chiffrer ou de déchiffrer le message.

### Un exemple bien connu : le chiffre de César

Cette méthode de chiffrement doit son nom à Jules César qui l'aurait utilisé pour communiquer à ses généraux.

Pour chiffrer à l'aide de cette méthode, on se donne un certain entier $n$. Il suffit de décaler toutes les lettres d'un message de $n$ rangs pour obtenir le message chiffré. Si jamais on dépasse la fin de l'alphabet avec ce procédé, il suffit alors de recommencer au début.

Par exemple, le message **Bonjour** chiffré avec un décalage de 3 lettres donne **Erqmrxu**.

![Chiffre de César](images/cesar.png)

La clé est ici le nombre n : pour déchiffrer un message, il suffit de reprendre le même procédé en prenant la lettre $n$ rangs avant celle du message chiffré.

### Chiffrement par substitution

Le chiffrement par substitution généralise le chiffrement de César. Dans ce cas, on remplace une lettre par une autre lettre de l'aphabet pour créer le message chiffré. Par exemple, si on remplace les lettres ABCDEFGHIJKLMNOPQRSTUVWXYZ par AZERTYUIOPQSDFGHJKLMWXCVBN, le message clair **SUBSTITUTION** devient **LWZLMOMWMOGF**.

Contrairement au chiffre de César, où il n'y a que 26 possibilités de décalage, le nombre de méthodes de chiffrement par substitution mono-alphabétique est de 26!, soit $4 \times 10^{26}$ environ.

### Chiffres de Vigenère (XVIe siècle) et Vernam (XXe siècle)

Une des faiblesses du chiffre par substitution est qu'une même lettre est toujours remplacé par la même autre lettre. Pour éviter ce désagrément, on peut utiliser le chiffre de Vigenère : on choisit la clé sous la forme d'un mot ou d'une phrase qui nous donnera le décalage à appliquer à chaque lettre du message.

Par exemple, si l'on choisit le mot CHAT comme clé, alors la première lettre sera décalée de 3 rangs, la suivante de 8 rangs, la suivante d'1 rang puis la suivante de 20 rangs, puis on recommence, 3, 8, 1, 20, 3, 8, 1, 20...

Cette méthode est plus robuste que le chiffrement monoalphabétique, mais demeure peu sécurisée si la clé est très courte par rapport au message.

Cette idée de décalage est reprise pour le chiffre de Vernam, en prenant quelques précautions sur le choix de la clé :

- La clé doit être une suite de caractères au moins aussi longue que le message à chiffrer.
- Les caractères composant la clé doivent être choisis de façon totalement aléatoire.
- Chaque clé, ou « masque », ne doit être utilisée qu’une seule fois (d’où le nom de masque jetable).

### Approche informatique

Prenons le cas du chiffre de Vigenère ou Vernam. Lorsque les données sont informatisées, donc mises sous forme binaire (par exemple en utilisant le format ASCII), la méthode se réduit à un calcul particulièrement simple, donc très rapide en pratique.

Le message en clair, à chiffrer, se présente comme une suite de bits. La clé est une autre suite de bits, de même longueur.

On traite un à un les bits du clair, en combinant chacun avec le bit de même rang dans la clé.

Appelons A un bit du clair et B le bit de même rang de la clé.

Le chiffrement consiste à calculer un bit C en effectuant sur A et B l'opération  « XOR », ou « OU EXCLUSIF », dont nous rappelons ici la table.

|A|B|$C=A \oplus B$|
|--|--|--|
|0|0|0|
|0|1|0|
|1|0|1|
|1|1|1|

L'opération est effectuée pour chaque bit du clair avec le bit correspondant de la clé.

Supposons que l'on souhaite chiffrer le message **Hello World!** à l'aide de la clé **toto**.

La chaîne de caractère **Hello world!** donne en binaire, en utilisant le code ASCII,

```010010000110010101101100011011000110111100100000010101110110111101110010011011000110010000100001```

La chaîne **toto** donne

```01110100011011110111010001101111```

Cette chaîne doit alors être reproduite pour être de même taille que le message original. On applique alors l'opérateur XOR à chaque couple de bits du message clair et de la clé de chiffrement.

![Opérateur XOR](images/c12c_1.jpg)

On obtient la suite de bits suivante :

```001111000000101000011000000000110001101101001111001000110000000000000110000000110001000001001110```

Cette suite de bits peut alors elle-même être reconvertie et exprimée à l'aide de caractères.

### Limites du chiffrement symétrique

Le chiffrement symétrique repose donc, comme nous l'avons vu, sur une clé, en général, un mot ou une phrase. L'utilisation d'un chiffrement symétrique suppose donc qu'avant d'entamer leur conversation, les interlocuteurs doivent eux-mêmes se mettre d'accord sur cette clé et la communiquer de manière discrète... et donc chiffrée elle-même...

## Chiffrement asymétrique

Le chiffrement 

!!! abstract "Chiffrement symétrique"

    Les méthode de chiffrement asymétrique utilisent deux clés :
    
    - une clé publique qui sert au chiffrement des messages et que l'on peut transmettre à n'importe qui
    - une clé privée que seul le récepteur du message possède et qui lui permettra de déchiffrer le message.

L'idée est la suivante : je souhaite communiquer avec d'autres personnes de manière sécurisée. Je fournis à chacune de ces personnes un coffret avec un cadenas (ma clé publique). Tout le monde est alors en mesure de m'envoyer un message sans qu'une personne qui intercepte la communication puisse comprendre son contenu.

En revanche, je suis le seul à détenir la clé du cadenas (ma clé privée). Seul moi pourrai donc ouvrir le coffret qui contient le message et donc le lire.

En vérité, il existe un lien entre la clé publique et la clé privée. L'un des algorithmes de chiffrement le plus célèbre est l'algorithme RSA (du nom de ses inventeurs Rivest Shamir et Adleman), utilisé notamment dansle commerce en ligne. Cet algorithme repose sur la factorisation d'un très grand nombre en produit de nombres premiers : la clé publique est un très grand nombre et la clé privée est l'un de ses facteurs premiers. Il est très facile, connaissant deux nombres d'en calculer le produit (et il est donc très facile de chiffrer un message). En revanche, étant donné un très grand nombre, il est très "difficile" de le factoriser : le déchiffrage d'un message chiffré par l'algorithme RSA est donc complexe.

