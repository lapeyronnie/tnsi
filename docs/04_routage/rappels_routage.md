---
author: Jason LAPEYRONNIE
title: Rappels et compléments de Première
---

## Adressage IP

Chaque « objet IP » est identifié par une adresse IP qui contient :

- l’adresse du réseau IP local (extraite grâce au « netmask » ou « masque de sous réseau ») ;
- le numéro de la machine dans le réseau IP local.

Une adresse IPv4 est un identifiant numérique à 32 bits (4 octets). Chaque « objet IP » est physiquement connecté à un réseau local de niveau 2 par ethernet, wifi ou bluetooth.

La communication avec d’autres « objets IP » appartenant au même réseau se fait directement via le réseau local de niveau 2 par l’intermédiaire d’un « switch » ou « commutateur ».
La communication avec d’autres « objets IP » d’autres réseaux IP distants se fait via des passerelles de niveau 3 ou routeurs.

![Principe des adresse IP](images/AdresseIP.png)

## Routage

Si l'hôte de destination se trouve sur le même réseau que l'hôte source, les paquets de données sont acheminés entre les deux hôtes sur le support local de niveau 2 via des commutateurs (« switchs ») sans nécessiter la présence d’un routeur. Les commutateurs (« switchs ») construisent une table d’adresses MAC des machines IP connectées à eux.

Ils se basent sur ces adresses MAC enregistrées pour commuter les trames.

Cependant, si l'hôte de destination et l'hôte source ne se trouvent pas sur le même réseau, le réseau local achemine le paquet de la source vers son routeur de passerelle de niveau 3. Le routeur examine la partie réseau de l'adresse de destination du paquet et achemine le paquet à l'interface appropriée. 

Si le réseau de destination est connecté directement à ce routeur, le paquet est transféré directement vers cet hôte. Si le réseau de destination n'est pas
connecté directement, le paquet est acheminé vers un second routeur qui constitue le routeur de tronçon suivant.

Le transfert du paquet devient alors la responsabilité de ce second routeur. De nombreux routeurs ou sauts tout au long du chemin peuvent traiter le paquet avant qu'il n'atteigne sa destination. Contrairement aux commutateurs (« switchs »), les routeurs se basent quant à eux sur les adresses IP pour transporter les données.

Aucun paquet ne peut être acheminé sans route. Que le paquet provienne d'un hôte ou qu'il soit acheminé par un routeur intermédiaire, le routeur a besoin d'une route pour savoir où l'acheminer. S'il n'existe aucune route vers un réseau de destination, le paquet ne peut pas être transféré. Les routeurs utilisent des tables de routage qui contiennent les routes qu'ils connaissent. Ces tables peuvent être construites manuellement (routage statique) ou automatiquement (routage dynamique). Dans ce cas, les routeurs s'appuient sur des protocoles spécifiques comme le protocole RIP (Routing Information Protocol) ou OSPF (Open Shortest Path First) par exemple.


![Principe des adresse IP](images/tableroutage.png)

## Exemple de communication

Considérons le réseau suivant

![Exemple de réseau](images/reseau.png)

**Cas n° 1** : M1 veut communiquer avec M3. Le paquet est envoyé de M1 vers le switch R1, R1 "constate" que M3 se trouve bien dans le réseau local 1, le paquet est donc envoyé directement vers M3. On peut résumer le trajet du paquet par : M1 → R1 → M3

**Cas n° 2** : M1 veut communiquer avec M6. Le paquet est envoyé de M1 vers le switch R1, R1 « constate » que M6 n’est pas sur le réseau local 1, R1 envoie donc le paquet vers le routeur A. Le routeur A n'est pas connecté directement au réseau local R2 (réseau local de la machine M6), mais il "sait" que le routeur B est connecté au réseau local 2. Le routeur A envoie le paquet vers le routeur B. Le routeur B est connecté au réseau local 2, il envoie le paquet au Switch R2. Le Switch R2 envoie le paquet à la machine M6. On a donc la route suivante : M1 → R1 → Routeur A → Routeur B → R2 → M6

**Cas n° 3** : M1 veut communiquer avec M9 Il existe souvent plusieurs chemins possibles pour relier 2 ordinateurs : M1 → R1 → Routeur A → Routeur B → Routeur D → Routeur E → R4 → M9 **OU** M1 → R1 → Routeur A → Routeur H → Routeur F → Routeur E → R4 → M9.
 
**Cas n°4** : M13 → R6 → Routeur G → Routeur F → Routeur E → R4 → M9 **OU** M13 → R6 → Routeur G → Routeur F → Routeur H → Routeur C → Routeur D → Routeur E → R4 → M9 . Le chemin n° 1 "Routeur F → Routeur E" est vraisemblablement plus rapide et donc préférable au chemin n° 2 "Routeur F → Routeur H". Cependant s’il y avait un problème technique entre le Routeur F et le Routeur E, l’existence du chemin "Routeur F → Routeur H" permettrait tout de même d’établir une communication entre M13 et M9. C’est l’existence de très nombreuses interconnexions et donc de routes possibles entre les différentes infrastructures qui rendent internet particulièrement peu sensible à des pannes localisées.

## Table de routage

Comment les commutateurs ou les routeurs procèdent-ils pour amener les paquets de données à bon port ? Dans le schéma du paragraphe précédent, les machines M1 et M4 n'ont pas la même adresse réseau car elles n'appartiennent pas au même réseau local. Si M1 cherche à entrer en communication avec M4 alors le switch R1 va constater que M4 n'appartient pas au réseau local (grâce à son adresse IP) et R1 va donc envoyer le paquet de données vers le routeur A. Cela sera donc au routeur A de gérer le "problème" : comment atteindre M4 ? Chaque routeur possède une table de routage. Une table de routage peut être vue comme un tableau qui va contenir des informations permettant au routeur d'envoyer le paquet de données dans la "bonne direction".

Prenons un nouvel exemple

![Exemple de réseau 2](images/reseau2.jpg)

La table de routage du routeur A doit contenir les informations suivantes :

- le routeur A est directement relié au réseau 172.168.0.0/16 par l'intermédiaire de son interface eth0
- le routeur A est directement relié au réseau 172.169.0.0/16 par l'intermédiaire de son interface eth2
- le routeur A est directement relié au réseau 192.168.7.0/24 par l'intermédiaire de son interface eth1 (le réseau 192.168.7.0/24 est un peu particulier car il est uniquement composé des routeurs A et G)
- le routeur A n'est pas directement relié au réseau 10.0.0.0/8 mais par contre il "sait" que les paquets à destination de ce réseau doivent être envoyé à la machine d'adresse IP 192.168.7.2/24 (c'est à dire le routeur G qui lui est directement relié au réseau 10.0.0.0/8)

On peut résumer les informations précédentes dans la table de routage simplifiée de A :

|Réseau|Moyen de l'atteindre|Métrique|
|---|---|---|
|172.168.0.0/16|eth0|0|
|192.168.7.0/24|eth1|0|
|172.169.0.0/16|eth2|0|
|10.0.0.0/8|192.168.7.2/24|1|

Dans un protocole de routage, la métrique est une mesure de la « distance » qui sépare un routeur d'un réseau de destination. En première approche on l’assimilera au nombre de sauts IP nécessaires pour atteindre le réseau destination. Ainsi, un réseau directement lié à un routeur aura une métrique de 0.

Il existe 2 méthodes pour remplir une table de routage : 

- le routage statique : chaque ligne doit être renseignée "à la main". Cette solution est seulement envisageable pour des très petits réseaux de réseaux 
- le routage dynamique : tout se fait "automatiquement", on utilise des protocoles qui vont permettre de "découvrir" les différentes routes automatiquement afin de pouvoir remplir la table de routage tout aussi automatiquement.